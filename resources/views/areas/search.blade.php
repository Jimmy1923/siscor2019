{!! Form::open(array('url' =>'area', 'method' => 'GET', 'autocomplete' => 'off', 'role' => 'search'))!!}

	<div class="form-group">
		<div class="input-group">
			<input type="text" name="searchText" class="form-control" value="{{ old('searchText')}}" placeholder="Buscar áreas...">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-raised btn-primary"><span class="glyphicon glyphicon-search"></span></button>
			</span>
		</div>
          	
    </div>
      
{!! Form::close()!!}