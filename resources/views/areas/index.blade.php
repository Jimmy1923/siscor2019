@extends('layouts.dashboard')

@section('content')
<div class="alert alert-dismissible alert-success" id="msj-success" style="display: none">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Área editada con éxito</strong>
</div>

<div class="sec_ttls">
	<h2><i class="material-icons">&#xE153;</i> Áreas</h2>
	<hr>
</div>

<div class="row panel panel-primary sec_toolbar">
	<div class="col-sm-8">
		<button type="button" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#simple-dialog">Nueva Área</button>
		@include('areas.create')
	</div>
	<div class="col-sm-4">@include('areas.search')</div>
</div>

<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Listado de áreas</h3>
	</div>
	<div class="panel-body table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th class="col-sm-2">Área</th>
		            <th class="col-sm-2">Línea de negocio</th>
		            <th class="col-sm-3">Opciones</th>
				</tr>
			</thead>
			<tbody>
		 			@foreach ($areas as $area)
		 				<tr>
		 					<td> {{$area->name}}</td>
		 					<td>{{$area->bussine->name}}</td>

				            <td>
				              <button class="btn_options ">
				                <a href="{{ route('area.edit', $area) }}" class="btn-text" ><i class="material-icons">edit</i>
				              </a>
				              </button>
				              {!! Form::open(['route' => ['area.destroy', $area]]) !!}
					              <button class="btn_options btn-danger btn" onClick="return confirm('Eliminar registro?')">
					                <i class="material-icons right ">&#xE92B;</i>
					              </button>
				              {!! Form::close() !!}
				            </td>
		 				</tr>
					@endforeach
			</tbody>
		</table>
	</div>
	<div  class=" text-center">
		{{ $areas->render()}}
	</div >
</div>

@endsection
