{!! Form::open(['route' => 'area.store'])!!}
{{ csrf_field()}}
<div class="modal" id="simple-dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Crear Nueva area</h4>
      </div>
      <div class="modal-body">

        <label for="name">Nombre del área</label>
        <input type="text" name="name" class="form-control" value="{{ old('name')}}">
        <br>
        <label>Selecciona una línea de negocio</label>
        <select name="category_id" class="form-control selectpicker" data-live-search="true">
            @foreach($bussines as $bussine)
              <option value="{{$bussine->id}}" >{{ $bussine->name}}</option>
              @endforeach
        </select>

      </div>

      <div class="modal-footer">
        <button  class="btn btn-raised btn-danger" data-dismiss="modal">Cancelar</button>
        <button  class="btn btn-raised btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>

{!! Form::close()!!}
