@extends('layouts.app')
@section('content')
<div class="container text-center">
	<div class="page-header">
		<h5>
			<i class="fa-fa-shopping-cart"></i>
			Areas <small>Editar Areas</small>
		</h5>
	</div>
	<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="page">
		  {!! Form::model($area, array('route' => array('area.update', $area)))!!}
		  	<input type="hidden" name="_method" value="PUT">
			<div class="form-group">
				<label class="control-label" for="bussine_id">Lineas de negocios</label>
				  {!! Form::select('bussine_id', $bussines, null, ['class' => 'form-control']) !!}               
            </div>

			<div class="form-group">
				<label for="name">Areas:</label>
				{!!
					Form::text(
					'name',
					null,
					array(
						'class'=>'form-control',
						'placeholder' => 'Ingresa el nombre..',
						'required' => 'required',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				{!!	Form::submit('Actualizar', array('class'=>'btn btn-primary')) !!}
				<a href="{{ route('area.index') }}" class="btn btn-warning">Cancelar</a>
			</div>
		{!! Form::close()!!}
		</div>
	</div>
</div>
</div>
@stop
