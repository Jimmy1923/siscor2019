<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="{{ config('svnshoop.meta.keywords')}}">
    <meta name="description" content="{{ config('svnshoop.meta.description')}}">
    <meta name="author" content="{{ config('svnshoop.meta.author')}}">
    <link rel="shortcut icon" href="{{ config('svnshoop.default_icon')}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/noty.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-material-datetimepicker.css')}}">
    @yield('dropzone')
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <!-- Page Layout here -->
   <div id="app">
        <header>
            @include('layouts.partials.nav')
        </header>
        <div class="container-fluid">
            <div class="">
                <div class="sec_menu cambio"> <!-- Note that "m4 l3" was added -->
                    <!-- Grey navigation panel-->
                    @include('layouts.partials.menu')

                </div>
                <div class="sec_dashboard"> <!-- Note that "m8 l9" was added -->
                    <!-- Teal page content-->
                    @include('layouts.partials.success')
                    @include('layouts.partials.errors')
                    @yield('content')
                    <notification :id="{{ Auth::id()}}"></notification>
                    <audio id="noty_audio">
                        <source src="{{ asset('audio/notify.mp3')}}">
                        <source src="{{ asset('audio/notify.ogg')}}">
                        <source src="{{ asset('audio/notify.wav')}}">
                    </audio>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js')}}"></script> 
    <script src="{{ asset('js/bootstrap-select.min.js')}}"></script>
    <script src="{{ asset('js/functions.js')}}"></script>
    <script src="{{ asset('js/main.js')}}"></script>
    <script src="{{ asset('js/requisitions.js')}}"></script>
    <script src="{{ asset('js/moment-with-locales.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{ asset('js/noty.js') }}"></script>
    @yield('scriptrequisition')
    <script>
        @if(Session::has('success'))
             new Noty({
                type: 'success',
                layout: 'topRight',
                text:'{{ Session::get('success')}}',
                timeout:5000,
                progressBar:true,
                closeWith:['click','button'],
                animation: {
                    open:'noty_effects_open',
                    close: 'noty_effects_close'
                },

            }).show();
        @endif
    </script>
</body>
</html>
