@if(count($errors) > 0)
	<div class="alert alert-dismissible alert-danger">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <strong>Ups!</strong>
	  @foreach($errors->all() as $error)
	  <li><a href="javascript:void(0)" class="alert-link">{{ $error}}</a></li>
	  @endforeach
	</div>

@endif