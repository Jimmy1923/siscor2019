<nav class="navbar">
    <div class="">
        <i class="btn_menu material-icons visible-xs-inline-block">&#xE5D2;</i>

        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav ">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="{{ route('login') }}">Login</a></li>
            @else
                <unread></unread>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                         <img class="circle_avatar" src="{{ Auth::user()->avatar}}" alt="icon">
                        {{ Auth::user()->full_name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/profile">Perfil</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>

