﻿<div class="panel panel-info">
    <div class="panel-heading text-center">
        <img class="logo_menu" src="{{ asset('images/logo_siscor-bl.png')}}" alt="">
                <i class="material-icons visible-xs-inline-block pull-right btn_close">&#xE5C9;</i>
    </div>

    <div class="panel-body">
        <div class="row sec_welcome">
            <div class="col-sm-3 text-center">
                <img class="circle_avatar" src="{{ Auth::user()->avatar}}" alt="avatar">
            </div>
            <div class="col-sm-9">
                <h5>Bienvenido</h5>
                <h4>{{ Auth::user()->name }}</h4>
            </div>
           
        </div>
        <ul class="nav nav-pills nav-stacked menu_ul">
            <li><a class="collection-item cambio" data-seccion="home" href="/home" ><i class="material-icons">&#xE871;</i> Panel de control</a></li>
            <li><a class="collection-item cambio" data-seccion="requisitions" href="/requisitions/create" ><i class="material-icons">&#xE89C;</i> Nueva Requisición</a></li>
            @if(auth()->user()->role_id == 2)
            <li><a class="collection-item cambio" data-seccion="provider" href="{{ route('provider.index') }}" ><i class="material-icons">&#xE563;</i> Proveedores</a>
            </li>
            @endif
            @if(auth()->user()->is_preathorize || auth()->user()->is_admin)
            <li><a href="/reports-general" class="collection-item"><i class="material-icons">&#xE873;</i>Generar Reportes</a></li>
            @endif
	    @if(auth()->user()->role_id == 4)
            	<li><a href="/reports-general" class="collection-item"><i class="material-icons">&#xE873;</i>Generar Reportes</a></li>
            @endif

            @if(auth()->user()->is_admin)
            <li><a class="collection-item cambio" data-seccion="company" href="{{ route('company.index') }}" ><i class="material-icons">&#xE0AF;</i> Empresas</a>
            </li>
            <li><a class="collection-item cambio" data-seccion="bussine" href="{{ route('bussine.index') }}" ><i class="material-icons">&#xE428;</i> Líneas de negocio</a>
            </li>
            <li><a class="collection-item cambio" data-seccion="area" href="{{ route('area.index') }}"><i class="material-icons">&#xE153;</i> Áreas</a>
            </li>
            <li><a class="collection-item cambio" data-seccion="provider" href="{{ route('provider.index') }}" ><i class="material-icons">&#xE563;</i> Proveedores</a>
            </li>
            <li><a class="collection-item cambio" data-seccion="categories" href="/categories" ><i class="material-icons">&#xE8E6;</i> Categorías</a>
            </li>
            <li><a class="collection-item cambio" data-seccion="subcategories" href="/subcategories" ><i class="material-icons">&#xE8E7;</i> Sub-Categorías</a>
            </li>
            <li><a class="collection-item cambio" data-seccion="users" href="/users" ><i class="material-icons">&#xE7FE;</i> Usuarios</a>
            </li>
            @endif
        </ul>
    </div>
</div>
