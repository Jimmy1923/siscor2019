<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="{{ config('svnshoop.meta.keywords')}}">
    <meta name="description" content="{{ config('svnshoop.meta.description')}}">
    <meta name="author" content="{{ config('svnshoop.meta.author')}}">
    <link rel="shortcut icon" href="{{ config('svnshoop.default_icon')}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
  

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>
<body>
   <!-- Page Layout here -->

    <div class="view_inicio">
        <div class="div_view_inicio">
            <a href="/"><img src="{{ asset('images/logo_siscor-bl2.png')}}" alt=""></a>
            <h3>SISTEMA DE COMPRAS Y REQUISICIONES</h2>
            <hr>
        </div>


        @yield('content')

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js')}}"></script>   
    <script>
        $.material.init()
    </script>

</body>
</html>
