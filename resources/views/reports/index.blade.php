@extends('layouts.dashboard')
@section('content')
{!! Form::open(['route'=>'searchreport.store','method'=>'POST' ]) !!}
<div class="panel panel-info">
	<div class="panel-heading">Generar Reportes</div>  
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<label>Selecciona una compañia</label>
				<select name="company" id="company_id" class="form-control selectpicker" data-live-search="true">
				<option  value="" selected>Seleccione una empresa</option>
					@foreach($companies as $company)
					<option value="{{$company->id}}" >{{ $company->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-6">
				<label>Selecciona un Negocio</label>
				<select name="bussine" id="bussine_id" class="form-control">
					
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<label for="created_at">Fecha de Inicio</label>
				<input type="date" name="created_at" placeholder="Selecione fecha de Inicio" class="form-control">
			</div>
			<div class="col-md-3">
				<label for="created_at">Fecha Final</label>
				<input type="date" name="finish_at" placeholder="Selecione fecha de Inicio" class="form-control">
			</div>
			<div class="col-md-4">
				<label for="status">Seleccione su Status</label>
				<select name="status" class="form-control selectpicker" data-live-search="true">
					<option>Seleccione un status</option>
					<option value="0">Validacion</option>
					<option value="1">Espera prea-autorizacion</option>
					<option value="2">Preautorizada</option>
					<option value="3">Autorizada</option>
					<option value="4">Proceso de pago</option>
					<option value="5">Pagada</option>
					<option value="6">Rechazada</option>
					<option value="7">Finalizada</option>
				</select>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="radio radio-primary">
						<label>
						<input type="radio" name="details" id="details" >
							Con Detalles
						</label>
					</div>
				</div>
				
				
			</div>
		</div>
		<button class="btn btn-raised btn-primary">Buscar</button>
	</div>
</div>
{!! Form::close()!!}
@endsection
