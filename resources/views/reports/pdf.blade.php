<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Impresión de requisición</title>
    <link rel="stylesheet" href="{{ asset('css/print.css')}}" media="all" />
  </head>
  <body>
    <header >
      <div id="logo">
        <img src="{{asset('images/logo.png')}}">
        <h3>SISTEMA DE COMPRAS Y REQUISICIONES</h3>
        @if($requisition->status == 4)
        <h3>Solicitud de Pago</h3>
        @endif
      </div>
      <h1>{{ $requisition->title}}</h1>
    </header>

    <div id="project">
      <div><span>FOLIO: </span> {{$requisition->folio}}</div>
      <div><span>EMPRESA: </span> {{$requisition->bussine->company->name}} </div>
      <div><span>LINEA DE NEGOCIO: </span> {{$requisition->bussine_name}}</div>
      <div><span>SOLICITANTE: </span> {{$requisition->user->name}} {{$requisition->user->lastname}}</div>
      <div><span>PROVEEDOR: </span> {{$requisition->provider_name}}</div>
      <div><span>DIRECCIÓN: </span> {{$requisition->provider_address}}</div>
      <div><span>RFC: </span> {{$requisition->provider_rfc}}</div>
      <div><span>FECHA: </span> {{ $date}}</div>
      @if($requisition->status == 0)
      <div><span>STATUS: </span>validacion</div>
      @elseif($requisition->status == 1)
      <div><span>STATUS: </span>En espera de pre-autorización</div>
      @elseif($requisition->status == 2)
      <div><span>STATUS: </span>Pre-autorizadas</div>
      @elseif($requisition->status == 3)
      <div><span>STATUS: </span>Autorizadas</div>
      @elseif($requisition->status == 4)
      <div><span>STATUS: </span>Proceso de pago</div>
      @elseif($requisition->status == 5)
      <div><span>STATUS: </span>Pagadas</div>
      @elseif($requisition->status == 6)
      <div><span>STATUS: </span>Rechazadas</div>
      @elseif($requisition->status == 7)
      <div><span>STATUS: </span>Finalizadas</div>
      @endif
    </div>


    <div class="sec_table">
      <table >
        <tr>
          <th>Articulo</th>
          <th>Cantidad</th> 
          <th>Unidad</th>
          <th>Tipo de pago</th>
          <th>Precio</th>
          <th>Iva</th>
          <th>Subtotal</th>
        </tr>
          @foreach($details as $detail)
        <tr>
            <td >{{ $detail->name}}</td>
            <td >{{ $detail->quantity}}</td>
            <td >{{ $detail->unity}}</td>
            <td >{{ $detail->type_payment}}</td>
            <td >{{ $detail->price}}</td>
            <td >{{ $detail->iva}}</td>
            <td >{{ $detail->quantity*$detail->price+$detail->iva}}</td>
        </tr>
          @endforeach
      <div class="sec_total"> <strong>TOTAL: {{ $requisition->total}}</strong></div>
      </table>


    </div>


    <footer>
      @ Todos los derechos reservados SISCOR, Villahermosa, Tabasco
    </footer>
  </body>
</html>