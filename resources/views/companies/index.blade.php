@extends('layouts.dashboard')

@section('content')

<div class="sec_ttls">
	<h2><i class="material-icons">&#xE0AF;</i> Empresas</h2>
	<hr>
</div>

<div class="row panel panel-primary sec_toolbar">
	<div class="col-sm-8">
		<button type="button" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#companies_create">Nueva Empresa</button>
  		@include('companies.create')
	</div>
	<div class="col-sm-4">
		@include('companies.search')
	</div>
</div>

<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Listado de Empresas</h3>
	</div>
	<div class="panel-body table-responsive">
	 	<table class="table table-striped table-bordered table-hover">
	 		<thead> 
	 			<tr>
	 				<th class="col-md-4">Nombre</th>
	 				<th class="col-md-5">Dirección</th>
	 				<th>RFC</th>
	 				<th>Opciones</th>
	 			</tr>
	 		</thead>
	 		<tbody>
	 			@foreach ($companies as $company)
	 				<tr>
	 					<td> {{$company->name}}</td>
	 					<td> {{$company->address}}</td>
	 					<td> {{$company->rfc}}</td>
	 					<td>
	 						<button class="btn_options ">
							<a href="{{ route('company.edit', $company) }}" class="btn-text">
								<i class="material-icons">edit</i>
							</a>
				            </button>

	 						{!! Form::open(['route' => ['company.destroy', $company]]) !!}
								<input type="hidden" name="_method" value="DELETE">
								<button onClick="return confirm('Eliminar registro?')" class="btn_options btn-danger btn">
									<i class="material-icons right ">&#xE92B;</i>
								</button>
    						{!! Form::close() !!}
	 					</td>
	 				</tr>
				@endforeach
	 		</tbody>
	 	</table>
	</div>

</div>

@endsection