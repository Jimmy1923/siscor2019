
{!! Form::open(['route' => 'company.store']) !!}
<div class="modal" id="companies_create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Crear Nueva Compañia</h4>
            </div>
            <div class="modal-body">
				<div class="form-group">
					<label for="name">Nombre:</label>
					{!!
						Form::text(
						'name',
						null,
						array(
							'class'=>'form-control',
							'required' => 'required',
							'autofocus' => 'autofocus'
							)
						)
					!!}
				</div> <br>
				<div class="form-group">
					<label for="direccion">Direccion:</label>
					{!!
						Form::textarea(
						'address',
						null,
						array(
							'class'=>'form-control',
							'required' => 'required',
							'autofocus' => 'autofocus'
							)
						)
					!!}
				</div><br>
				<div class="form-group">
					<label for="rfc">RFC:</label>
					{!!
						Form::text(
						'rfc',
						null,
						array(
							'class'=>'form-control',
							'required' => 'required',
							'autofocus' => 'autofocus'
							)
						)
					!!}
				</div><br>
				<div class="form-group text-right">
					<a href="{{ route('company.index') }}" class="btn btn-raised btn-danger">Cancelar</a>
					{!!	Form::submit('Guardar', array('class'=>'btn btn-raised btn-success')) !!}
				</div>
			</div>
        </div>
    </div>
</div>

{!! Form::close()!!}

