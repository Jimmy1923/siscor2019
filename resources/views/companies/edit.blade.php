@extends('layouts.app')
@section('content')
<div class="container text-center">
	<div class="page-header">
		<h5>
			<i class="fa-fa-shopping-cart"></i>
			Compañias <small>Editar Compañias</small>
		</h5>
	</div>
	<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="page">
			{!! Form::model($company, array('route' => array('company.update', $company)))!!}

			<input type="hidden" name="_method" value="PUT">
			<div class="form-group">
				<label for="name">Nombre:</label>
				{!!
					Form::text(
					'name',
					null,
					array(
						'class'=>'form-control',
						'placeholder' => 'Ingresa el nombre..',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				<label for="direccion">Direccion:</label>
				{!!
					Form::textarea(
					'address',
					null,
					array(
						'class'=>'form-control'
						)
					)
				!!}
			</div>
			<div class="form-group">
				<label for="rfc">RFC:</label>
				{!!
					Form::text(
					'rfc',
					null,
					array(
						'class'=>'form-control',
						'placeholder' => 'Ingrese su RFC..',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				{!! Form::submit('Actualizar', array('class'=>'btn btn-primary')) !!}
				<a href="{{ route('company.index') }}" class="btn btn-warning">Cancelar</a>
			</div>
		{!! Form::close()!!}
		</div>
	</div>
</div>
</div>
@stop
