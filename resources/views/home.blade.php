@extends('layouts.dashboard')

@section('content')

@if(auth()->user()->is_gerente)
	@include('requisitions.index-gerente')
@else
	@include('requisitions.index')
@endif

@endsection
