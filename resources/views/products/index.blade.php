@extends('layouts.dashboard')
@section('title', 'Lista de Categorias')

@section('content')
<div class="row">
	<div class="col-md-8">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-category">Crear productos</button>
		  @include('products.create')
		@include('products.search')		
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Listado de productos</h3>
				
			</div>
			<div class="panel-body">
				<table class="table table-striped table-hover ">
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre Completo</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
					@foreach($product as $products)
						<tr>
							<td>{{ $products->id}}</td>
							<td>{{ $products->name}}</td>
							<td>
							
								<a href="#" data-toggle="modal" data-target="#edit-products{{$products->id}}"><i class="material-icons">edit</i></a>
								{{-- <a href="categories/{{$products->id}}/destroy" class="btn btn-danger btn-fab btn-fab-mini"><i class="material-icons">delete</i></a> --}}
							
							</td>
						</tr>
						<form action="index.php">
						@include('products.edit')
						</form>
					@endforeach
					</tbody>
				</table>
			</div>
			<ul class="pagination">
				{{ $product->render()}}
			</ul>
		</div>
	</div>
</div>
@endsection