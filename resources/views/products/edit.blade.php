{!! Form::open(['route' => ['product.update',$products], 'method' => 'PUT'])!!}
          {{ csrf_field()}} 
          
<div class="modal" id="edit-products{{$products->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Editar producto {{ $products->name}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                   <label for="name">Nombre del producto</label>
                   <input type="text" name="name" class="form-control" value="{{ old('name', $products->name)}}" placeholder="Escribe el producto..">
               </div>
            </div>
            <div class="modal-footer">
              <button  class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

 {!! Form::close()!!} 