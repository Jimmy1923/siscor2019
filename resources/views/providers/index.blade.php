@extends('layouts.dashboard')

@section('content')

<div class="sec_ttls">
	<h2><i class="material-icons">&#xE563;</i> Proveedores</h2>
	<hr>
</div>

<div class="row panel panel-primary sec_toolbar">
	<div class="col-sm-8">
		<button type="button" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#create-proveedores">Nuevo proveedores</button>
  		@include('providers.create')
	</div>
	<div class="col-sm-4">@include('providers.search')</div>
</div>


<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Listado de Proveedores</h3>
	</div>
	<div class="panel-body table-responsive">
	 	<table class="table table-striped table-bordered table-hover">
	 		<thead> 
	 			<tr>
	 				<th>Nombre</th>
	 				<th>Dirección</th>
	 				<th>RFC</th>
	 				<th>Telefono</th>
	 				<th>Email</th>
	 				<th>Opciones</th>
	 			</tr>
	 		</thead>
	 		<tbody>
	 			@foreach ($providers as $provider)
	 				<tr>
	 					<td> {{$provider->name}}</td>
	 					<td> {{$provider->address}}</td>
	 					<td> {{$provider->rfc}}</td>
	 					<td> {{$provider->phone}}</td>
	 					<td> {{$provider->email}}</td>
	 					<td>
	 						<button class="btn_options ">
								<a href="{{ route('provider.edit', $provider) }}" class="btn-text">
									<i class="material-icons">edit</i>
								</a>
				            </button>
	 						@if(auth()->user()->role_id == 1)
                                {!! Form::open(['route' => ['provider.destroy', $provider]]) !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button onClick="return confirm('Eliminar registro?')" class="btn_options btn-danger btn">
                                            <i class="material-icons right ">&#xE92B;</i>
                                        </button>
                                {!! Form::close() !!}
                            @endif
	 					</td>
	 				</tr>
				@endforeach
	 		</tbody>
	 	</table>
	</div>
	<div  class=" text-center">
		{{ $providers->render()}}
	</div >
</div>








@endsection