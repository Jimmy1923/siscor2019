@extends('layouts.app')
@section('content')
<div class="container text-center">
	<div class="page-header">
		<h5>
			<i class="fa-fa-shopping-cart"></i>
			Proveedores <small>Agregar Proveedores</small>
		</h5>
	</div>
	<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="page">
			{!! Form::model($provider, array('route' => array('provider.update', $provider)))!!}

			<input type="hidden" name="_method" value="PUT">
			<div class="form-group">
				<label for="name">Nombre:</label>
				{!!
					Form::text(
					'name',
					null,
					array(
						'class'=>'form-control',
						'placeholder' => 'Ingresa el nombre..',
						'required' => 'required',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				<label for="direccion">Direccion:</label>
				{!!
					Form::textarea(
					'address',
					null,
					array(
						'class'=>'form-control',
						'placeholder' => 'Ingresa la dirección..',
						'required' => 'required',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				<label for="rfc">RFC:</label>
				{!!
					Form::text(
					'rfc',
					null,
					array(
						'class'=>'form-control',
						'placeholder' => 'Ingrese su RFC..',
						'required' => 'required',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				<label for="rfc">Telefono:</label>
				{!!
					Form::text(
					'phone',
					null,
					array(
						'class'=>'form-control',
						'placeholder' => 'Ingrese su telefono..',
						'required' => 'required',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				<label for="rfc">email:</label>
				{!!
					Form::text(
					'email',
					null,
					array(
						'class'=>'form-control',
						'placeholder' => 'Ingrese su email..',
						'required' => 'required',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				{!!	Form::submit('Actualizar', array('class'=>'btn btn-primary')) !!}
				<a href="{{ route('provider.index') }}" class="btn btn-warning">Cancelar</a>
			</div>
		{!! Form::close()!!}
		</div>
	</div>
</div>
</div>
@stop
