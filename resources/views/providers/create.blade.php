{!! Form::open(['route' => 'provider.store'])!!}
{{ csrf_field()}}
<div class="modal" id="create-proveedores">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Crear Nueva Categoria</h4>
            </div>
            <div class="modal-body">
				<div class="form-group">
					<label for="name">Nombre:</label>
					{!!
						Form::text(
						'name',
						null,
						array(
							'class'=>'form-control',
							'placeholder' => 'Ingresa el nombre..',
							'required' => 'required',
							'autofocus' => 'autofocus'
							)
						)
					!!}
				</div>
				<div class="form-group">
					<label for="direccion">Direccion:</label>
					{!!
						Form::textarea(
						'address',
						null,
						array(
							'class'=>'form-control',
							'placeholder' => 'Ingresa la dirección..',
							'required' => 'required',
							'autofocus' => 'autofocus'
							)
						)
					!!}
				</div>
				<div class="form-group">
					<label for="rfc">RFC:</label>
					{!!
						Form::text(
						'rfc',
						null,
						array(
							'class'=>'form-control',
							'placeholder' => 'Ingrese su RFC..',
							'required' => 'required',
							'autofocus' => 'autofocus'
							)
						)
					!!}
				</div>
				<div class="form-group">
					<label for="rfc">Telefono:</label>
					{!!
						Form::text(
						'phone',
						null,
						array(
							'class'=>'form-control',
							'placeholder' => 'Ingrese su telefono..',
							'required' => 'required',
							'autofocus' => 'autofocus'
							)
						)
					!!}	
				</div>
				<div class="form-group">
					<label for="rfc">email:</label>
					{!!
						Form::email(
						'email',
						null,
						array(
							'class'=>'form-control',
							'placeholder' => 'Ingrese su email..',
							'required' => 'required',
							'autofocus' => 'autofocus'
							)
						)
					!!}
				</div>
            </div>
            <div class="text-right m_footer">
                <div class="form-group">
					<a href="{{ route('provider.index') }}" class="btn btn-raised btn-danger">Cancelar</a>
					{!!	Form::submit('Guardar', array('class'=>'btn btn-raised btn-success')) !!}
				</div>
                
            </div>
        </div>
    </div>
</div>

{!! Form::close()!!}


