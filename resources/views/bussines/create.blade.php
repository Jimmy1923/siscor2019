@extends('layouts.dashboard')
@section('content')
<div class="container text-center">
	<div class="page-header">
		<h5>
			<i class="fa-fa-shopping-cart"></i>
			Negocios <small>Agregar Negocios</small>
		</h5>
	</div>
	<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="page">
		  {!! Form::open(['route'=>'bussine.store','method'=>'POST']) !!}
			<div class="form-group">
				<label class="control-label" for="company_id">Compañias</label>
				{!! Form::select('company_id', $companies, null, ['class' => ' selectpicker', 'data-live-search' => 'true']) !!}                 
            </div>

			<div class="form-group">
				<label for="name">Nombre de la línea de negocio:</label>
				{!!
					Form::text(
					'name',
					null,
					array(
						'class'=>'form-control',
						'required' => 'required',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				{!!	Form::submit('Guardar', array('class'=>'btn btn-primary')) !!}
				<a href="{{ route('bussine.index') }}" class="btn btn-warning">Cancelar</a>
			</div>
		{!! Form::close()!!}
		</div>
	</div>
</div>
</div>
@stop
