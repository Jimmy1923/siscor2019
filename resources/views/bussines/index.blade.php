@extends('layouts.dashboard')

@section('content')

<div class="sec_ttls">
	<h2><i class="material-icons">&#xE428;</i> Líneas de negocios</h2>
	<hr>
</div>

<div class="row panel panel-primary sec_toolbar">
	<div class="col-sm-8">
		<button type="button" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#bussine-create">Nueva Línea de negocio</button>
	</div>
	<div class="col-sm-4">
	</div>
</div>



	<div class="page-header">
		<h5>
			<i class="fa-fa-shopping-cart"></i>
			Negocios <a href="{{ route('bussine.create')}}" class="btn btn-warning"><i class="fa fa-plus-circle"></i>Negocios+</a>
		</h5>
	</div>




<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Listado de areas</h3>
	</div>
	<div class="panel-body table-responsive">
	 	<table class="table table-striped table-bordered table-hover">
	 		<thead> 
	 			<tr>
	 				<th>Empresa</th>
	 				<th>Linea de negocio</th>
	 				<th>Opciones</th>
	 			</tr>
	 		</thead>
	 		<tbody>
	 			@foreach ($bussines as $bussine)
	 				<tr>
	 					<td><h6> {{$bussine->company->name}}</h6></td>
	 					<td><h6> {{$bussine->name}}</h6></td>
	 					<td>
	 						<button class="btn_options ">
								<a href="{{ route('bussine.edit', $bussine) }}" class="btn-text">
									<i class="material-icons">edit</i>
								</a>
				            </button>
	 						{!! Form::open(['route' => ['bussine.destroy', $bussine]]) !!}
								<input type="hidden" name="_method" value="DELETE">
								<button onClick="return confirm('Eliminar registro?')" class="btn_options btn-danger btn">
									<i class="material-icons right ">&#xE92B;</i>
								</button>
    						{!! Form::close() !!}
	 					</td>
	 				</tr>
				@endforeach
	 		</tbody>
	 	</table>
	</div>
	<div  class=" text-center">
		{{ $bussines->render()}}
	</div >
</div>


@endsection