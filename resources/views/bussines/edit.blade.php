@extends('layouts.app')
@section('content')
<div class="container text-center">
	<div class="page-header">
		<h5>
			<i class="fa-fa-shopping-cart"></i>
			Negocios <small>Editar Negocios</small>
		</h5>
	</div>
	<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="page">
		  {!! Form::model($bussine, array('route' => array('bussine.update', $bussine)))!!}
		  	<input type="hidden" name="_method" value="PUT">

			<div class="form-group">
				<label class="control-label" for="company_id">Compañias</label>
				{!! Form::select('company_id', $companies, null, ['class' => 'form-control']) !!}                 
            </div>

			<div class="form-group">
				<label for="name">Nombre:</label>
				{!!
					Form::text(
					'name',
					null,
					array(
						'class'=>'form-control',
						'placeholder' => 'Ingresa el nombre..',
						'required' => 'required',
						'autofocus' => 'autofocus'
						)
					)
				!!}
			</div>
			<div class="form-group">
				{!!	Form::submit('Actualizar', array('class'=>'btn btn-primary')) !!}
				<a href="{{ route('bussine.index') }}" class="btn btn-warning">Cancelar</a>
			</div>
		{!! Form::close()!!}
		</div>
	</div>
</div>
</div>
@stop
