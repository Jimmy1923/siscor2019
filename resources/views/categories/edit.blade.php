<div class="modal" id="edit-category">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Editar Categoria </h4>
            </div>
            <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token()}}" id="token">
                    <input type="hidden" id="id">
                <div class="form-group">
                   <label for="name">Nombre de la categoria</label>
                   <input type="text" name="name" id="name" class="form-control" value="{{ old('name')}}" placeholder="Escribe la categoria..">
               </div>
            </div>
            <div class="modal-footer">
              <button  class="btn btn-primary" id="actualizar">Guardar</button>
            </div>
        </div>
    </div>
</div>

