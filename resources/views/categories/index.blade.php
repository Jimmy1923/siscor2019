@extends('layouts.dashboard')
@section('title', 'Lista de Categorias')

@section('content')
<div class="alert alert-dismissible alert-success" id="msj-success" style="display: none">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Categoria editada con éxito</strong>
</div>

<div class="sec_ttls">
	<h2><i class="material-icons">&#xE8E6;</i> Categorías</h2>
	<hr>
</div>

<div class="row panel panel-primary sec_toolbar">
	<div class="col-sm-8">
		<button type="button" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#create-category">Nueva Categoría</button>
		  @include('categories.create')
	</div>
	<div class="col-sm-4">@include('categories.search')</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Listado de Categorias</h3>
			</div>
			<div class="panel-body table-responsive">
				<table class="table table-striped table-bordered table-hover" id="categories">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th>Nombre Completo</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
					@foreach($categories as $category)
						<tr>
							<td class="text-center">{{ $category->id}}</td>
							<td>{{ $category->name}}</td>
							<td>
								<button class="btn_options btn-text" data-toggle="modal" data-target="#edit-category" OnClick="mostrar(this);" value="{{ $category->id}}"><i class="material-icons">edit</i></button>
							</td>
						</tr>
						@include('categories.edit')
					@endforeach
					</tbody>
				</table>
			</div>
			<div  class="pagination text-center">
				{{ $categories->render()}}
			</div >
		</div>
	</div>
</div>
@endsection