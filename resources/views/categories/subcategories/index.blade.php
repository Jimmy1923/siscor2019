@extends('layouts.dashboard')
@section('title', 'Lista de Categorias')

@section('content')
<div class="alert alert-dismissible alert-success" id="msj-success" style="display: none">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Sub-categoria editada con éxito</strong>
</div>

<div class="sec_ttls">
	<h2><i class="material-icons">&#xE8E7;</i> Subcategorías</h2>
	<hr>
</div>

<div class="row panel panel-primary sec_toolbar">
	<div class="col-sm-8">
		<button type="button" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#simple-dialog">Nueva Subcategoría</button>
		  @include('categories.subcategories.create')
	</div>
	<div class="col-sm-4">@include('categories.subcategories.search')</div>
</div>

<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Listado de Categorías</h3>
	</div>
	<div class="panel-body table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th class="text-center">#</th>
					<th>Nombre Completo</th>
					<th>Categoría</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
			@foreach($subcategories as $sub)
				<tr>
					<td class="text-center">{{ $sub->id}}</td>
					<td>{{ $sub->name}}</td>
					<td>{{ $sub->category_name}}</td>
					<td>
					
						<button class="btn_options btn-text" data-toggle="modal" data-target="#edit-sub" OnClick="mostrarSub(this);" value="{{ $sub->id}}"><i class="material-icons">edit</i></button>
						{{-- <a href="categories/{{$category->id}}/destroy" class="btn_options btn-text"><i class="material-icons">delete</i></a> --}}
					
					</td>
				</tr>
				<tr>
				@include('categories.subcategories.edit')
			@endforeach
			</tbody>
		</table>
	</div>
	<div  class=" text-center">
		{{ $subcategories->render()}}
	</div >
</div>


@endsection