{!! Form::open(['route' => 'subcategories.store'])!!}
{{ csrf_field()}}
<div class="modal" id="simple-dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Crear Nueva Sub-categoria</h4>
      </div>
      <div class="modal-body">
        <label for="name">Nombre de la categoria</label>
         <input type="text" name="name" class="form-control" value="{{ old('name')}}" placeholder="Escribe la sub-categoria..">
             <label>Selecciona una categoria</label>

         <select name="category_id" class="form-control selectpicker" data-live-search="true">
      @foreach($categories as $category)
        <option value="{{$category->id}}" >{{ $category->name}}</option>
        @endforeach
    </select>
      </div>
      <div class="modal-footer">
        <button  class="btn btn-raised btn-danger" data-dismiss="modal">Cancelar</button>
        <button  class="btn btn-raised btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>

{!! Form::close()!!}
