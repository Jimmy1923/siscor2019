<div class="modal" id="edit-sub">
    <div class="modal-dialog" id="cerrar">
        <div class="modal-content">
            <div class="modal-header">
                <button  class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Editar Categoria {{ $sub->name}}</h4>
            </div>
            <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token()}}" id="token">
                    <input type="hidden" id="id" value="{{ $sub->id}}">
              <div class="form-group">
                  <h4>Editar la subcategoria {{ $sub->name}}</h4>
                  <label for="name">Nombre de la subcategoria</label>
                  <input type="text" name="name" id="name" class="form-control" value="{{ old('name', $sub->name)}}" placeholder="Escribe la categoria..">
              </div>
              <div class="form-group">
                <label>Selecciona una categoria</label>
                  <select class="form-control selectpicker" name="category_id" id="category_selected">
                      @foreach($categories as $category)
                      <option value="{{$category->id}}" @if($sub->category_id == $category->id) selected @endif >{{ $category->name}}</option>
                      @endforeach
                  </select>
              </div>
              <div class="modal-footer">
                <button  class="btn btn-primary" id="actualizar-sub">Guardar</button>
            </div>
        </div>
    </div>
</div>