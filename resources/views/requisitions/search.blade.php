{!! Form::open(array('url' =>'requisitions', 'method' => 'GET', 'autocomplete' => 'off', 'role' => 'search'))!!}

	<div class="form-group">
		<div class="input-group">
			<input type="text" name="searchText" class="form-control" value="{{ old('searchText')}}" placeholder="Buscar Requisición...">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-raised btn-primary">Buscar</button>
			</span>
		</div>
          	
    </div>
      
{!! Form::close()!!}