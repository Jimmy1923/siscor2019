
@extends('layouts.dashboard')
@section('title', 'Lista de requisiciones')
@section('dropzone')
<link rel="stylesheet" href="{{ asset('css/dropzone.css')}}">
@endsection
@section('content')

<div class="sec_ttls">
  <h2>Detalles Requisición: "{{ $requisition->title}}"</h2>
  <hr>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Detalles generales</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
		                  	<label for="title">Folio</label>
		                  	<input type="text" name="title" value="{{$requisition->folio}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	              	<div class="col-md-2">
						<div class="form-group">
		                  	<label for="title">Categoria</label>
		                  	<input type="text" name="title" value="{{$dRequisition->category_name}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	              	<div class="col-md-2">
						<div class="form-group">
		                  	<label for="title">Subcategoria</label>
		                  	<input type="text" name="title" value="{{$dRequisition->subcategory_name}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	              	<div class="col-md-2">
						<div class="form-group">
		                  	<label for="title">Fecha Programada de pago</label>
		                  	<input type="text" name="title" value="{{$requisition->date_programming}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	              	<div class="col-md-2">
						<div class="form-group">
		                  	<label for="title">Fecha de pago</label>
		                  	<input type="text" name="title" value="{{$requisition->fechadepago}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	              	<div class="col-md-2">
						<div class="form-group">
		                  	<label for="title">Tipo de Pago</label>
		                  	<input type="text" name="title" value="{{$dRequisition->type_payment}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	            </div>
	            <div class="row">
	            	<div class="col-md-2">
	            		<label for="title">Estado</label>
		                <input type="text" name="title" value="{{$requisition->state}}" class="form-control" readonly="readonly">
	            	</div>
	            	<div class="col-md-2">
	            		<label for="title">Validó</label>
		                <input type="text" name="title" value="{{$requisition->validation_name}}" class="form-control" readonly="readonly">
	            	</div>
	            	<div class="col-md-2">
	            		<label for="title">Pre-autorizó</label>
		                <input type="text" name="title" value="{{$requisition->preauthorize_name}}" class="form-control" readonly="readonly">
	            	</div>
	            	<div class="col-md-2">
	            		<label for="title">Autorizó</label>
		                <input type="text" name="title" value="{{$requisition->authorize_name}}" class="form-control" readonly="readonly">
	            	</div>
	            	<div class="col-md-2">
	            		<label for="title">Pagó</label>
		                <input type="text" name="title" value="{{$requisition->payment_name}}" class="form-control" readonly="readonly">
	            	</div>
	            	<div class="col-md-2">
	            		<label for="title">Rechazó</label>
		                <input type="text" name="title" value="{{$requisition->rejected_name}}" class="form-control" readonly="readonly">
	            	</div>
	            </div>
	        </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Datos del Solicitante</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					{{-- <div class="col-md-6">
						<div class="form-group">
		                  	<label for="title">Empresa</label>
		                  	<input type="text" name="title" value="{{$requisition->company_name}}" class="form-control" readonly="readonly">
		                </div>
	              	</div> --}}
	              	<div class="col-md-12">
						<div class="form-group">
		                  	<label for="title">Línea de negocio</label>
		                  	<input type="text" name="title" value="{{$requisition->bussine_name}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	            </div>
	            <div class="row">
					<div class="col-md-6">
						<div class="form-group">
		                  	<label for="title">Nombre</label>
		                  	<input type="text" name="title" value="{{$requisition->user_name}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	              	<div class="col-md-6">
						<div class="form-group">
		                  	<label for="title">Área</label>
		                  	<input type="text" name="title" value="{{$requisition->area_name}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	            </div>
	        </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Datos del Proveedor</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
		                  	<label for="title">Nombre</label>
		                  	<input type="text" name="title" value="{{$requisition->provider_name}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	              	<div class="col-md-6">
						<div class="form-group">
		                  	<label for="title">RFC</label>
		                  	<input type="text" name="title" value="{{$requisition->provider_address}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	            </div>
	            <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Dirección Fiscal</label>
                            <input type="text" name="title" value="{{$requisition->provider_rfc}}" class="form-control" readonly="readonly">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="phone">Telefono</label>
                            <input type="text" id="phone" name="phone" value="{{$requisition->provider->phone}}" class="form-control" readonly="readonly" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" value="{{$requisition->provider->email}}" class="form-control" readonly="readonly" >
                        </div>
                    </div>
                </div>
	        </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Datos de la Requisición</h3>
			</div>
			<div class="panel-body">
              	<div class="row">
              		<div class="col-sm-12 col-xs-12 table-responsive">
						<table id="details" class="table table-striped table-bordered table-condensed table-hover">
							<thead>
								<th>Fecha de Creación</th>
								<th>Articulo</th>
								<th>Cantidad</th>
								<th>Unidad</th>
								<th>Precio</th>
								<th>Iva</th>
								<th>Subtotal</th>
							</thead>
							<tfoot>
								<th colspan="5"></th>
								<th><strong>Total</strong></th>
								<th><strong>$ {{ number_format($requisition->total, 2)}}</strong></th>
							</tfoot>
							<tbody>
								@foreach($details as $detail)
									<tr>
										<td>{{ $detail->created_at}}</td>
										<td>{{ $detail->name}}</td>
										<td>{{ $detail->quantity}}</td>
										<td>{{ $detail->unity}}</td>
										<td>$ {{ number_format($detail->price, 2)}}</td>
										<td>$ {{ number_format($detail->iva, 2)}}</td>
										<td>$ {{ number_format($detail->quantity*$detail->price+$detail->iva, 2)}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
              	</div>
              	<div class="row sec_notas">
              		<div class="col-sm-4 text-center notas">
              		<p>Cotizaciones</p>
              			@foreach($requisition->archives as $image)
              				<a href="{{ url('/requisiciones/'.$image->archive)}}" target="_blank"><i class="material-icons btn-text">cloud_download</i></a>
              			@endforeach
              		</div>
              		<div class="col-sm-4 text-center notas">
              		<p>Comprobante de pago</p>
              			<a href="{{ url('/requisiciones/tickets/'.$requisition->archive_payment)}}" target="_blank"><i class="material-icons btn-text">cloud_download</i></a>
              		</div>
              		<div class="col-sm-4 text-center notas">
              		<p>Comprobante de recibido</p>
              			<a href="{{ url('/requisiciones/vouchers/'.$requisition->archive_ticket)}}" target="_blank"><i class="material-icons btn-text">cloud_download</i></a>
              		</div>
              	</div>
              	<div class="row">
              	@if($requisition->status != 6 || $requisition->status != 7)
              		@include('requisitions.partials.nextstatus')
              	</div>
              	@endif
           	</div>
        </div>
    </div>
</div>
<div class="">
	<comment :user="{{ auth()->user() }}" :requisition="{{ $requisition }}"></comment>
</div>
@endsection
@section('scriptrequisition')
	<script src="{{ asset('js/dropzone.js')}}"></script>
	<script>
		$("#payment").hide();
        Dropzone.options.addTicket = {
            autoProcessQueue: true,
            dictDefaultMessage: "Arrastra tu comprobante de pago aquí",
            maxFilezise: 10,
            maxFiles: 1,

            success: function(file, response) {
            	if (file.status == 'success') {
            		handleDropzoneFileUpload.handleSuccess(response);
            		$("#payment").show();
            	}else {
            		handleDropzoneFileUpload.handleError(response);
            		$("#payment").hide();
            	}
            }
        };
        /*automatic file upload*/
        var handleDropzoneFileUpload = {
        	handleError: function(response) {
        		console.log(response);
        	},
        	handleSuccess: function(response){

        	}
        };

        $("#voucher").hide();
        Dropzone.options.addVoucher = {
            autoProcessQueue: true,
            dictDefaultMessage: "Arrastra tu ticket de recibido aquí",
            maxFilezise: 10,
            maxFiles: 1,

            success: function(file, response) {
            	if (file.status == 'success') {
            		handleDropzoneFileUpload.handleSuccess(response);
            		$("#voucher").show();
            	}else {
            		handleDropzoneFileUpload.handleError(response);
            		$("#voucher").hide();
            	}
            }
        };
        /*automatic file upload*/
        var handleDropzoneFileUpload = {
        	handleError: function(response) {
        		console.log(response);
        	},
        	handleSuccess: function(response){

        	}
        };

        
    </script>

@endsection