<div class="row">
<div class="col-md-12 requisitions">
	
	<div class="sec_ttls">
	  <h2><i class="material-icons">&#xE7FE;</i> Panel de Control</h2>
	  <hr>
	</div>

	<div class="row panel panel-primary sec_toolbar">
	  	<div class="col-sm-6">
			<label>Selecciona una compañia</label>
	        <select name="company" id="company" class="form-control selectpicker" data-live-search="true">
               <option value="" selected>Seleccione un empresa</option>
				@foreach($companies as $company)
				    <option value="{{$company->id}}" >{{ $company->name}}</option>
		        @endforeach
			</select>
		</div>
		<div class="col-sm-6">
			<label>Selecciona un Negocio</label>
			<select name="bussine" id="list-of-bussines" class="form-control">
			</select>
		</div>
	</div>
</div>

</div>
<div class="row">
	<div class="caja_out">
		
		<div class="col-md-12 caja_in">
			<ul class="nav nav-tabs ">
				<li class="cambio active"><a class="a_nav" href="#validation" data-toggle="tab">En validación</a></li>
				<li class="cambio"><a class="a_nav" href="#wait" data-toggle="tab">En espera de pre-autorización</a></li>
				<li class="cambio"><a class="a_nav" href="#pre-authorize" data-toggle="tab">Pre-autorizadas</a></li>
				<li class="cambio"><a class="a_nav" href="#authorize" data-toggle="tab">Autorizadas</a></li>
				<li class="cambio"><a class="a_nav g" href="#payment" data-toggle="tab">Proceso de pago</a></li>
				<li class="cambio"><a class="a_nav" href="#paid" data-toggle="tab">Pagadas</a></li>
				<li class="cambio"><a class="a_nav" href="#rejected" data-toggle="tab">Rechazadas</a></li>
				<li class="cambio"><a class="a_nav" href="#finish" data-toggle="tab">Finalizadas</a></li>
			</ul>
			<div id="myTabContent" class="tab-content">
				<div class="tab-pane fade active in" id="validation">
					@include('requisitions.partials.table-validation')
				</div>
				<div class="tab-pane fade" id="wait">
					@include('requisitions.partials.table-wait')
				</div>
				
				<div class="tab-pane fade" id="pre-authorize">
					@include('requisitions.partials.table-pre-authorize')
				</div>
				<div class="tab-pane fade" id="authorize">
					@include('requisitions.partials.table-authorize')
				</div>
				<div class="tab-pane fade" id="payment">
					@include('requisitions.partials.table-payment')
				</div>
				<div class="tab-pane fade" id="paid">
					@include('requisitions.partials.table-paid')
				</div>
				<div class="tab-pane fade" id="rejected">
					@include('requisitions.partials.table-rejected')
				</div>
				<div class="tab-pane fade" id="finish">
					@include('requisitions.partials.table-finish')
				</div> 
			</div>
		</div>
	
	</div>

</div>
