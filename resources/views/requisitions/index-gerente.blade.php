<div class="row">
	<div class="col-md-12 ">
		<ul class="nav nav-tabs" style="margin-bottom: 15px;">
			<li class="active"><a href="#validation" data-toggle="tab">En validación</a></li>
			<li class="active"><a href="#wait" data-toggle="tab">En espera de pre-autorización</a></li>
			<li><a href="#pre-authorize" data-toggle="tab">Pre-autorizadas</a></li>
			<li class="active"><a href="#authorize" data-toggle="tab">Autorizadas</a></li>
			<li><a href="#payment" data-toggle="tab">Proceso de pago</a></li>
			<li class="active"><a href="#paid" data-toggle="tab">Pagadas</a></li>
			<li><a href="#rejected" data-toggle="tab">Rechazadas</a></li>
			<li><a href="#finish" data-toggle="tab">Finalizadas</a></li>
		</ul>
		<div id="myTabContent" class="tab-content">
			<div class="tab-pane fade active in" id="validation">
				@include('requisitions.partials.table-validation')
			</div>
			<div class="tab-pane fade" id="wait">
				@include('requisitions.partials.table-wait')
			</div>
			
			<div class="tab-pane fade" id="pre-authorize">
				@include('requisitions.partials.table-pre-authorize')
			</div>
			<div class="tab-pane fade" id="authorize">
				@include('requisitions.partials.table-authorize')
			</div>
			<div class="tab-pane fade" id="payment">
				@include('requisitions.partials.table-payment')
			</div>
			<div class="tab-pane fade" id="paid">
				@include('requisitions.partials.table-paid')
			</div>
			<div class="tab-pane fade" id="rejected">
				@include('requisitions.partials.table-rejected')
			</div>
			<div class="tab-pane fade" id="finish">
				@include('requisitions.partials.table-finish')
			</div> 
		</div>
	</div>
</div>

    
