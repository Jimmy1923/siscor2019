{!! Form::open(['route' => 'provider.store']) !!}
{{ csrf_field()}}
<div class="modal" id="create-provider">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Crear Nuevo Proveedor</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                   <label for="name">Nombre del Proveedor</label>
                   <input type="text" name="name" class="form-control" value="{{ old('name')}}" placeholder="Escribe el nombre">
               </div>
               <div class="form-group">
                   <label for="address">Dirección</label>
                   <input type="text" name="address" class="form-control" value="{{ old('address')}}" placeholder="Escribe la dirección">
               </div>
               <div class="form-group">
                   <label for="rfc">RFC</label>
                   <input type="text" name="rfc" class="form-control" value="{{ old('rfc')}}" placeholder="Escribe el RFC..">
               </div>
               <div class="form-group">
                   <label for="phone">Telefono</label>
                   <input type="text" name="phone" class="form-control" value="{{ old('phone')}}" placeholder="Escribe el phone..">
               </div>
               <div class="form-group">
                   <label for="email">Email</label>
                   <input type="email" name="email" class="form-control" value="{{ old('email')}}" placeholder="Escribe el RFC..">
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Cancelar</button>
                <button class="btn btn-raised btn-success">Guardar</button>
                
            </div>
        </div>
    </div>
</div>

{!! Form::close()!!}