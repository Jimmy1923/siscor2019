<table class="table table-striped table-hover ">
  <thead>
  <tr>
    <th>Folio</th>
    <th>Fecha de creación</th>
    <th>Concepto</th>
    <th>Fecha programada de pago</th>
    <th>Total</th>
    <th>Status</th>
    <th>Empresa</th>
    <th>Detalles</th>
  </tr>
  </thead>
  <tbody>
  <tr>
  @foreach ($validations as $requisition)
    <td>{{$requisition->folio}}</td>
    <td>{{$requisition->created_at}}</td>
    <td>{{$requisition->title}}</td>
    <td>{{$requisition->date_programming}}</td>
    <td>${{ number_format($requisition->total, 2)}}</td>
    <td>{{$requisition->state}}</td>
    <td>{{ $requisition->bussine_name}}</td>
    <td><a href="{{ route('requisitions.show', $requisition) }}">
          <i class="btn_detalles material-icons">&#xE5D3;</i>
        </a>
    </td>
  </tr>
  @endforeach
  </tbody>
</table>