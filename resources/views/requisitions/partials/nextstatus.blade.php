@if(auth()->user()->is_validation && $requisition->status == 0)
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/validate" class="btn btn-raised btn-success">Validar</a>
    </div>
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/rejected" class="btn btn-raised btn-danger">Rechazar</a>
    </div>
@endif
@if(auth()->user()->is_preathorize && $requisition->status == 1)
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/preauthorize" class="btn btn-raised btn-success">Pre-autorizar</a>
    </div>
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/rejected" class="btn btn-raised btn-danger">Rechazar</a>
    </div>
@endif
@if(auth()->user()->is_authorize && $requisition->status == 2)
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/authorizar" class="btn btn-raised btn-success">Autorizar</a>
    </div>
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/rejected" class="btn btn-raised btn-danger">Rechazar</a>
    </div>
@endif

@if(auth()->user()->is_process && $requisition->status == 3)
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/process" class="btn btn-raised btn-success">Proceso Pago</a>
    </div>
@endif
@if(auth()->user()->is_payment && $requisition->status == 4)
{!! Form::open(['route'=>'requisitions.uploadticket','method'=>'POST','enctype' => 'multipart/form-data', 'id' => 'addTicket','class' =>'dropzone' ]) !!}
    <input type="hidden" name="requisition_id" value="{{ $requisition->id }}">
   
{!! Form::close()!!}
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/payment" class="btn btn-raised btn-success" id="payment">Pagar</a>
    </div>
    
@endif

@if(auth()->user()->role_id ==6 || auth()->user()->role_id ==8 || auth()->user()->role_id ==7 && $requisition->status == 5 && $requisition->user_id == auth()->user()->id)
{!! Form::open(['route'=>'requisitions.uploadvoucher','method'=>'POST','enctype' => 'multipart/form-data', 'id' => 'addVoucher','class' =>'dropzone' ]) !!}
    <input type="hidden" name="requisition_id" value="{{ $requisition->id }}">
   
{!! Form::close()!!}
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/finalizadas" class="btn btn-raised btn-primary" id="voucher">Finalizar</a>
    </div>
@endif
@if(auth()->user()->is_validation  && $requisition->status == 5 && $requisition->user_id == auth()->user()->id )
{!! Form::open(['route'=>'requisitions.uploadvoucher','method'=>'POST','enctype' => 'multipart/form-data', 'id' => 'addVoucher','class' =>'dropzone' ]) !!}
    <input type="hidden" name="requisition_id" value="{{ $requisition->id }}">
   
{!! Form::close()!!}
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/finalizadas" class="btn btn-raised btn-primary" id="voucher">Finalizar</a>
    </div>
@endif
@if(auth()->user()->is_preathorize  && $requisition->status == 5 && $requisition->user_id == auth()->user()->id )
{!! Form::open(['route'=>'requisitions.uploadvoucher','method'=>'POST','enctype' => 'multipart/form-data', 'id' => 'addVoucher','class' =>'dropzone' ]) !!}
    <input type="hidden" name="requisition_id" value="{{ $requisition->id }}">
   
{!! Form::close()!!}
    <div class="col-md-2">
        <a href="/requisitions/{{ $requisition->id}}/finalizadas" class="btn btn-raised btn-primary" id="voucher">Finalizar</a>
    </div>
@endif
@if($requisition->status == 3 || $requisition->status == 0 || $requisition->status == 1 || $requisition->status == 2 || $requisition->status == 4 || $requisition->status == 5 || $requisition->status == 6 || $requisition->status == 7)
<div class="col-md-2">
    <a href="/export-requisition/{{ $requisition->id}}/print" class="btn btn-raised btn-primary" target="_blank">
        @if($requisition->status == 3 || $requisition->status == 0 || $requisition->status == 1 || $requisition->status == 2 || $requisition->status == 4 || $requisition->status == 5 || $requisition->status == 6 || $requisition->status == 7)
        Imprimir
        @else 
        Solicitud de pago
        @endif
    </a>
</div>
@endif
