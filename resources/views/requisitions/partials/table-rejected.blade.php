<table class="table table-striped table-hover ">
  <thead>
  <tr>
    <th>Folio</th>
    <th>Fecha de creación</th>
    <th>Concepto</th>
    <th>Fecha programada de pago</th>
    <th>Total</th>
    <th>Status</th>
    <th>Empresa</th>
  </tr>
  </thead>
  <tbody>
  <tr>
  @foreach ($rejecteds as $pre)
    <td>{{$pre->folio}}</td>
    <td>{{$pre->created_at}}</td>
    <td>{{$pre->title}}</td>
    <td>{{$pre->date_programming}}</td>
    <td>${{ number_format($pre->total, 2)}}</td>
    <td>{{$pre->state}}</td>
    <td>{{ $pre->bussine_name}}</td>
    <td><a href="{{ route('requisitions.show', $pre) }}">
          <i class="btn_detalles material-icons">&#xE5D3;</i>
          
        </a>
    </td>
  </tr>
  @endforeach
  </tbody>
</table>