{!! Form::open(['route' => 'product.store'])!!}
{{ csrf_field()}}
<div class="modal" id="create-product">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Crear Nuevo Producto</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                   <label for="name">Nombre del Producto</label>
                   <input type="text" name="name" class="form-control" value="{{ old('name')}}" placeholder="Escribe el nombre">
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Cancelar</button>
                <button class="btn btn-raised btn-success">Guardar</button>
                
            </div>
        </div>
    </div>
</div>

{!! Form::close()!!}