@extends('layouts.dashboard')
@section('title', 'Lista de requisiciones')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-primary">

			<div class="panel-heading">
			<h3 class="panel-title">Detalles de la requisicion</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
		                  	<label for="title">Concepto</label>
		                  	<input type="text" name="title" value="{{$requisition->title}}" class="form-control" readonly="readonly">
		                </div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="title">Fecha programada de pago</label>
	                  		<input type="text" name="date_programming" value="{{$requisition->created_at}}" readonly="readonly" class="form-control">
	              		</div>
              		</div>
				</div>
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Datos del solicitante</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">			
	                  	<div class="form-group">
	              			<label for="empresa">Empresa</label>
	                      	<input type="text" name="empresa" id="empresa" class="form-control" readonly="readonly" value="{{$requisition->companies}}">
	              		</div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="address">Linea de negocio</label>
	                  		<input type="text" name="address" id="address" class="form-control" readonly="readonly" value="{{$requisition->bussines}}">
	              		</div>
	              	</div>
				</div>
              	<div class="row">
              		<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="name">Nombre</label>
	                  		<input type="text" name="name" id="name" class="form-control" readonly="readonly" value="{{$requisition->users}}">
	              		</div>
              		</div>
              		<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="area">Area</label>
	                  		<input type="text" name="area" id="area" class="form-control" readonly="readonly" value="{{$requisition->area}}">
	              		</div>
              		</div>
              	</div>
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Datos del Proveedor</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
	                  	<div class="form-group">
	              			<label for="providers">Proveedor</label>
	                      	<input type="text" name="providers" id="providers" class="form-control" readonly="readonly" value="{{$requisition->providers}}">
	              		</div>
	              	</div>
	              	<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="address">Dirección</label>
	                  		<input type="text" name="address" id="address" class="form-control" readonly="readonly" value="{{$requisition->providers_addre}}">
	              		</div>
	              	</div>
				</div>
              	<div class="row">
              		<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="rfc">RFC</label>
	                  		<input type="text" name="rfc" id="rfc" class="form-control" readonly="readonly" value="{{$requisition->providers_rfc}}">
	              		</div>
              		</div>
              		@include('requisitions.partials.create-provider')
              	</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Datos de la Requisición</h3>
			</div>
			<div class="panel-body">
			<input type="text" name="provider" id="provider" class="form-control" style="display:none">
              	<div class="row">
              		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<table id="details" class="table table-striped table-bordered table-condensed table-hover">
							<thead>
								<th>Opciones</th>
								<th>Concepto</th>
								<th>Tipo</th>
								<th>Formas de Pago</th>
								<th>Unidad</th>
								<th>Precio</th>
								<th>Cantidad</th>
								<th>IVA</th>
								<th>Subtotal</th>
							</thead>
							<tfoot>
								<th>X</th>
								<th>Computadora</th>
								<th>Equipo de computo</th>
								<th>Efectivo</th>
								<th>10</th>
								<th>$ 1000.00</th>
								<th>10</th>
								<th>16</th>
								<th><h4 id="total">$. 0.00</h4><input type="hidden" name="total_sale" id="total_sale"></input></th>
							</tfoot>
							<tbody></tbody>
						</table>
					</div>
              	</div>
              	<div class="row">
              		<div class="col-md-6">
              			<!--<div class="form-group">
						  <input type="file" id="inputFile4" multiple="">
						  <div class="input-group">
						    <input type="text" readonly="" class="form-control" placeholder="Selecciona tu Cotización">
						      <span class="input-group-btn input-group-sm">
						        <button type="button" class="btn btn-fab btn-fab-mini">
						          <i class="material-icons">attach_file</i>
						        </button>
						      </span>
						</div>
						</div>-->
	              		<div class="form-group">
						  <div class="col-md-12">
			              		<div class="form-group">
			              			<label for="rfc">Requisicion</label>
			                  		<a href="../../requisiciones/pdf/{{$requisition->archive_payment}}"><input type="text" name="rfc" id="rfc" class="form-control" readonly="readonly" value="{{$requisition->archive_payment}}"></a>
			              		</div>
              				</div>
						</div>
              		</div>
              	</div>   
            {!! Form::open(['route' => ['requisitions.update',$requisition], 'method' => 'PUT'])!!}
            <input type="text" name="status" id="status" class="form-control" style="display:none" value="2">
              	<div class="row">
              		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="save">
						<div class="form-group">
							<input type="hidden" name="_token" value="{{ csrf_token()}}">
							{!!	Form::submit('PRE-AUTORIZAR', array('class'=>'btn btn-primary')) !!}
							<button class="btn btn-raised btn-danger" type="reset">RECHAZAR</button>
						</div>
					</div>
              	</div>
			</div>
		{!! Form::close()!!}
		</div>
	</div>
</div>	
@endsection