﻿@extends('layouts.dashboard')
@section('title', 'Lista de Categorias')
@section('dropzone')
<link rel="stylesheet" href="{{ asset('css/dropzone.css')}}">
@endsection

@section('content')

<div class="sec_ttls">
  <h2><i class="material-icons">&#xE7FE;</i> Nueva Requisición</h2>
  <hr>
</div>


{!! Form::open(['route'=>'requisitions.store','method'=>'POST','enctype' => 'multipart/form-data', 'id' => 'my-dropzone','class' =>'dropzone' ]) !!}
@if( $user->role_id   == 7 || $user->bussine_id == 26)
<div class="row new_req">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Empresas</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-6">
						<label>Selecciona una compañia</label>
	        			<select name="company_ids" id="company_ids" class="form-control selectpicker" data-live-search="true">
               			<option value="" selected>Seleccione un empresa</option>
							@foreach($companies as $company)
				    			<option value="{{$company->id}}" >{{ $company->name}}</option>
		        			@endforeach
						</select>
					</div>
					<div class="col-sm-6">
						<label>Selecciona un Negocio</label>
						<select name="list-of-bussines_ids" id="list-of-bussines_ids" class="form-control">
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
<div class="row new_req">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Datos del Proveedor</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
	              			<label for="address">Proveedor</label>
	                  		<select class="form-control selectpicker" name="provider_id" data-live-search="true" id="provider_id">
	                      <option value="" selected>Seleccione un proveedor</option>
	                      @foreach($providers as $provider)
	                      <option value="{{ $provider->id}}">{{ $provider->name}}</option>
	                      @endforeach
	                  	</select>
	              		</div>
	              	</div>
	              	<div class="col-sm-6">
	              		<div class="form-group">
	              			<label for="address">Dirección</label>
	                  		<input type="text" id="address" name="address" class="form-control">
	              		</div>
	              	</div>
				</div>
              	<div class="row">
              		<div class="col-sm-6">
	              		<div class="form-group">
	              			<label for="rfc">RFC</label>
	                  		<input type="text" id="rfc" name="rfc" class="form-control">
	              		</div>
              		</div>
					<div class="col-sm-6">
	              		<div class="form-group">
	              			<label for="phone">Telefono</label>
	                  		<input type="text" id="phone" name="phone" class="form-control">
	              		</div>
              		</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
	              		<div class="form-group">
	              			<label for="email">Email</label>
	                  		<input type="text" id="email" name="email" class="form-control">
	              		</div>
              		</div>
              		<div class="col-md-6">
              			<div class="form-group">
              				<a class="btn btn-primary" data-toggle="modal" data-target="#create-provider">Agregar Proveedor</a>
              			</div>
              		</div>
              	</div>
			</div>
		</div>
	</div>
</div>
<div class="row new_req">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Datos de la Requisición</h3>
			</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
	              		<div class="form-group">
	              			<label for="product">Artículo</label>
	              			<select name="product_id" id="product_id" class="form-control selectpicker" data-live-search="true">
						      @foreach($products as $product)
						        <option value="{{$product->id}}_{{ $product->price_promedio}}" >{{ $product->name}}</option>
						        @endforeach
						    </select>
	              		</div>
	              	</div>
	              	<div class="col-md-2">
	              		<div class="form-group">
	              			<a class="btn btn-raised btn-primary" data-toggle="modal" data-target="#create-product">Agregar Producto</a>
	              		</div>
	              	</div>
					
				</div>
              	<div class="row">
              		<div class="col-md-8">
						<div class="form-group">
		                  	<label for="title">Concepto</label>
		                  	<input type="text" name="title" value="{{ old('title')}}" class="form-control" placeholder="Concepto o nombre de la requisición">
		                </div>
	              	</div>
              		<div class="col-md-4">
	              		<div class="form-group">
	              			<label for="date_programming">Fecha programada de pago</label>
	                  		<input type="date" id="date" name="date_programming" class="form-control" placeholder="Fecha Programada de Pago" value="{{ old('date_programming')}}">

	              		</div>
              		</div>
				</div>
              	<div class="row">
              		<div class="col-md-3">
	              		<div class="form-group">
	              		<label>Selecciona una categoria</label>
						    <select name="category_id" id="category_id" class="form-control selectpicker" data-live-search="true">
						    <option value="" selected>Seleccione una categoria</option>
						      @foreach($categories as $category)
						        <option value="{{$category->id}}" >{{ $category->name}}</option>
						        @endforeach
						    </select>
	              		</div>
	              	</div>
              		<div class="col-md-3">
	              		<div class="form-group">
	              			<label for="subcategory_id">Tipo</label>
	              			<select class="form-control " name="subcategory_id" id="subcategory_id">
	              				<option value="placeholder">Selecciona</option>
	              			</select>
	              		</div>
              		</div>
              		<div class="col-md-3">
	              		<div class="form-group">
	              			<label for="type_payment">Forma de Pago</label>
	                  		<select class="form-control selectpicker" id="type_payment" name="type_payment">
	                  			<option value="0">Efectivo</option>
	                  			<option value="1">Cheque</option>
	                  			<option value="2">Transferencia</option>
						<option value="3">Pago en linea</option>
	                  		</select>
	              		</div>
	              	</div>
	              	<div class="col-md-3">
	              		<div class="form-group">
	              			<label for="unity">Unidad de medida</label>
						    <select name="unity" id="unity" class="form-control selectpicker" data-live-search="true">
						        <option value="0" >Piezas</option>
						        <option value="1" >Cajas</option>
						        <option value="2" >Litros</option>
						        <option value="3" >Kilos</option>

						    </select>
	              		</div>
	              	</div>
              	</div>
              	<div class="row">
              		<div class="col-md-3">
	              		<div class="form-group">
	              			<label for="price">Precio</label>
	              			<input type="number" id="price_product" name="price" class="form-control">
	              		</div>
              		</div>
              		<div class="col-md-3">
	              		<div class="form-group">
	              			<label for="quantity">Cantidad</label>
	                  		<input type="number" id="quantity" name="quantity" class="form-control">
	              		</div>
              		</div>
              		<div class="col-md-3">
	              		<div class="form-group">
	              			<label for="iva">iva</label>
	                  		<input type="number" id="iva" name="iva" class="form-control">
	              		</div>
	              	</div>
	              	
              		<div class="col-md-3">
	              		<div class="form-group">
	              			<button type="button" id="btn_add" class="btn btn-raised btn-primary">Agregar</button>
	              		</div>
              		</div>
              	</div>
              	<div class="row">
              		<div class="col-md-12 table-responsive">
						<table id="details" class="table table-striped table-bordered table-hover">
							<thead>
								<th>Opciones</th>
								<th>Articulo</th>
								<th>Tipo</th>
								<th>Formas de Pago</th>
								<th>Unidad</th>
								<th>Precio</th>
								<th>Cantidad</th>
								<th>IVA</th>
								<th>Subtotal</th>
							</thead>
							<tfoot>
								<th colspan="7"></th>
								<th>Total</th>
								<th><h4 id="total">$. 0.00</h4><input type="hidden" name="total" id="total_re"></input></th>
							</tfoot>
							<tbody></tbody>
						</table>
					</div>
              	</div>
              	<div class="row">
              		<div class="col-md-12">
              			<div class="fallback">
    						 <div class="dz-message" >
	                        		Arrastra tus cotizaciones aquí
	                    	</div>
	                    	<div class="dropzone-previews"></div>
	                    </div>
              		</div>
              	</div>             	
              	<div class="row">
              		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="save">
						<div class="form-group">
							<input type="hidden" name="_token" value="{{ csrf_token()}}">
							<button class="btn btn-raised btn-success" id="submit">Guardar</button>
							<button class="btn btn-raised btn-danger"><a href="/requisitions/create">Cancelar</a></button>
						</div>
					</div>
              	</div>
			</div>
		{!! Form::close()!!}
		@include('requisitions.partials.create-provider')
		@include('requisitions.partials.create-product')
		</div>
	</div>
</div>	
@endsection

@section('scriptrequisition')
	<script src="{{ asset('js/dropzone.js')}}"></script>
	<script src="{{ asset('js/new-requisition.js')}}"></script>
	<script>
        Dropzone.options.myDropzone = {
            autoProcessQueue: false,
            uploadMultiple: true,
            dictDefaultMessage: "Arrastra tus cotizaciones aquí",
            maxFilezise: 10,
            maxFiles: 5,
            
            init: function() {
                var submitBtn = document.querySelector("#submit");
                myDropzone = this;
                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {
                    alert("Archivo Cargado");
                });
                
                this.on("complete", function(file) {
                    myDropzone.removeFile(file);
                    new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text:'Nueva requisición en proceso de validación',
                                timeout:5000,
                                progressBar:true,
                                closeWith:['click','button'],
                                animation: {
                                    open:'noty_effects_open',
                                    close: 'noty_effects_close'
                                },

                            }).show();
                    
					location.href = '/home';
                });
 
                this.on("success", 
                    myDropzone.processQueue.bind(myDropzone)

                );
            }
        };
    </script>

@endsection