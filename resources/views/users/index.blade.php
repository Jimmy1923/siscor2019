@extends('layouts.dashboard')
@section('title', 'Listado de usuarios')

@section('content')

<div class="sec_ttls">
  <h2><i class="material-icons">&#xE7FE;</i> Usuarios</h2>
  <hr>
</div>

<div class="row panel panel-primary sec_toolbar">
  <div class="col-sm-8">
      <a href="/users/create" class="btn btn-raised btn-primary">Nuevo Usuario</a>
  </div>
  <div class="col-sm-4">@include('users.partials.search')</div>
</div>

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Listado de Usuarios</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
            <th>Nombre Completo</th>
            <th>Correo</th>
            <th>Tipo de Usuario</th>
            <th>Línea de Negocio</th>
            <th>Área</th>
            <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->full_name}}</td>
            <td>{{$user->email}}</td>
            <td>{{ $user->tipo_user}}</td>
            <td>{{$user->bussine_name}}</td>
            <td>{{ $user->area_name}}</td>
            <td>
              <button class="btn_options ">
                <a href="/users/{{$user->id}}/edit" class="btn-text" data-position="top" data-delay="50" data-tooltip="Cuenta"><i class="material-icons">edit</i>
              </a>
              </button>
              <button class="btn_options">
                <a href="/users/{{ $user->id}}" class=" btn-text" data-position="top" data-delay="50" data-tooltip="Cuenta"><i class="material-icons right">&#xE5D3;</i>
              </a>
              </button>
              
            </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class=" text-center">
    {{ $users->render()}}
  </div >
</div>

@endsection