@extends('layouts.dashboard')
@section('title', 'Creación de usuarios')

@section('content')


<div class="sec_ttls">
  <h2><i class="material-icons">&#xE7FE;</i>Nuevo Usuario</h2>
  <hr>
</div>

<div class="new_user panel panel-primary">
    <div class="panel-heading">Datos del nuevo usuario</div>  
    <div class="panel-body">
        {!! Form::open(['route' => 'users.store'])!!}
        {{ csrf_field()}}

        <div class="row">
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" value="{{ old('email')}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                   <label for="password">Contraseña</label>
                   <input type="text" name="password" class="form-control" value="{{ old('password', str_random(8))}}">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name')}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                    <label for="lastname">Apellidos</label>
                    <input type="text" name="lastname" class="form-control" value="{{ old('lastname')}}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                    <label for="address">Dirección</label>
                    <input type="text" name="address" class="form-control" value="{{ old('address')}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                    <label for="phone">Teléfono</label>
                    <input type="text" name="phone" class="form-control" value="{{ old('phone')}}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group groups_bottom">
                   <label for="role_id">Seleccione un rol</label>
                    <select class="form-control" name="role_id">
                        @foreach($roles as $rol)
                        <option value="{{ $rol->id}}">{{ $rol->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group groups_bottom">
                    <label for="bussine_id">Seleccione una línea de Negocio</label>
                    <select class="form-control" name="bussine_id">
                        @foreach($bussines as $bussine)
                        <option value="{{ $bussine->id}}">{{ $bussine->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group groups_bottom">
                    <label for="area_id">Seleccione un Área</label>
                    <select class="form-control" name="area_id">
                        @foreach($areas as $area)
                        <option value="{{ $area->id}}">{{ $area->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group groups_bottom">
            <button class="btn btn-raised btn-success">Registrar</button>
        </div>

        {!! Form::close()!!}
    </div>
</div>
@endsection