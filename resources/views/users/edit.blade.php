@extends('layouts.dashboard')
@section('title', 'Edición de usuarios')

@section('content')
<div class="panel panel-primary">
  <div class="panel-heading">
    Editar usuario {{ $user->full_name}}
  </div>  
  <div class="panel-body">
      
    {!! Form::open(['route' => ['users.update', $user], 'method' => 'PUT'])!!}          
    {{ csrf_field()}}

        <div class="row">
            <div class="col-md-12">
                <div class="form-group groups_bottom">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" value="{{ old('email', $user->email)}}">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name',$user->name)}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                    <label for="lastname">Apellidos</label>
                    <input type="text" name="lastname" class="form-control" value="{{ old('lastname',$user->lastname)}}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                    <label for="address">Dirección</label>
                    <input type="text" name="address" class="form-control" value="{{ old('address', $user->address)}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group groups_bottom">
                    <label for="phone">Teléfono</label>
                    <input type="text" name="phone" class="form-control" value="{{ old('phone', $user->phone)}}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group groups_bottom">
                   <label for="role_id">Seleccione un rol</label>
                    <select class="form-control" name="role_id">
                        @foreach($roles as $rol)
                        <option value="{{ $rol->id}}" @if($user->role_id == $rol->id) selected @endif>{{ $rol->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group groups_bottom">
                    <label for="bussine_id">Seleccione una línea de Negocio</label>
                    <select class="form-control" name="bussine_id">
                        @foreach($bussines as $bussine)
                        <option value="{{ $bussine->id}}" @if($user->bussine_id == $bussine->id) selected @endif>{{ $bussine->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group groups_bottom">
                    <label for="area_id">Seleccione un Área</label>
                    <select class="form-control" name="area_id">
                        @foreach($areas as $area)
                        <option value="{{ $area->id}}" @if($user->area_id == $area->id) selected @endif>{{ $area->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-raised btn-success">Guardar</button>
        </div>

    {!! Form::close()!!}
</div>
</div>
@endsection