@extends('layouts.dashboard')
@section('title', 'Perfil del usuario')

@section('content')

<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">Configurar Perfil</div>
        <div class="panel-body">
        {!! Form::open(['url' =>'/profile','method' => 'POST','enctype' => 'multipart/form-data']) !!}
              {!! csrf_field() !!}
        	<div class="form-group">
			  <input type="file" id="avatar" name="avatar" multiple="">
			  <div class="input-group">
			    <input type="text" readonly="" class="form-control" placeholder="Sube tu foto aquí">
			      <span class="input-group-btn input-group-sm">
			        <button type="button" class="btn btn-fab btn-fab-mini">
			          <i class="material-icons">add_a_photo</i>
			        </button>
			      </span>
			  </div>
			</div>
			<div class="form-group">
				<input type="password" name="current_password" placeholder="Contraseña acutal" class="form-control">
			</div>
			<div class="form-group">
				<input type="password" name="password" placeholder="Nueva Contraseña" class="form-control">
			</div>
			<div class="form-group">
				<input type="password" name="password_confirmation" placeholder="Confirmar Contraseña" class="form-control">
			</div>
			<button class="btn btn-raised btn-primary">Guardar Cambios</button>
           
        {!! Form::close()!!}
        </div>
    </div>
</div>
		
@endsection