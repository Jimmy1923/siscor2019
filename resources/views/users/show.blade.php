@extends('layouts.dashboard')
@section('title', 'Creación de usuarios')

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">Detalles del usuario {{ $user->full_name}}</div>  
    <div class="panel-body">
      <div class="col-md-8">
          <div class="form-group">
            <label for="email">Email:</label>
            {{ $user->email}}
          </div>
          <div class="form-group">
            <label for="name">Nombre:</label>
            {{ $user->name}}
          </div>
          <div class="form-group">
            <label for="lastname">Apellidos:</label>
            {{$user->lastname}}
          </div>
          <div class="form-group">
            <label for="address">Dirección:</label>
            {{$user->address}}
          </div>
          <div class="form-group">
            <label for="phone">Teléfono:</label>
            {{$user->phone}}
          </div>
          
          <div class="form-group">
            <label for="role_id">Rol:</label>
            {{ $user->tipo_user}}
          </div>
          <div class="form-group">
            <label for="bussine_id">Línea de Negocio:</label>
            {{$user->bussine_name}}
          </div>
          <div class="form-group">
            <label for="area_id">Área:</label>
            {{$user->area_name}}
          </div>
      </div>
      <div class="col-md-4 text-center">
        <img class="circle_avatar2" src="{{ Auth::user()->avatar}}" alt="avatar">
      </div>

 
    </div>
</div>



 
@endsection