<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisitions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folio');
            $table->string('title');
            $table->string('description')->nullable();
            $table->boolean('status');
            $table->string('archive_payment')->nullable();
            $table->string('archive_ticket')->nullable();
            $table->decimal('total', 11, 2);
            $table->string('fechadepago')->nullable();
            $table->date('date_programming');
            $table->integer('area_id')->unsigned();
            $table->foreign('area_id')->references('id')->on('areas');
            $table->integer('bussine_id')->unsigned();
            $table->foreign('bussine_id')->references('id')->on('bussines');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('validation_id')->unsigned()->nullable();
            $table->foreign('validation_id')->references('id')->on('users');
            $table->integer('preauthorize_id')->unsigned()->nullable();
            $table->foreign('preauthorize_id')->references('id')->on('users');
            $table->integer('authorize_id')->unsigned()->nullable();
            $table->foreign('authorize_id')->references('id')->on('users');
            $table->integer('payment_id')->unsigned()->nullable();
            $table->foreign('payment_id')->references('id')->on('users');
            $table->integer('rejected_id')->unsigned()->nullable();
            $table->foreign('rejected_id')->references('id')->on('users');
            $table->integer('finish_id')->unsigned()->nullable();
            $table->foreign('finish_id')->references('id')->on('users');
            $table->integer('provider_id')->unsigned()->nullable();
            $table->foreign('provider_id')->references('id')->on('providers');
            
            $table->timestamps();
        });

        Schema::create('details_requisitions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products'); 
            $table->string('type_payment');
            $table->integer('quantity');
            $table->string('unity');
            $table->decimal('iva', 11, 2);
            $table->decimal('price', 11, 2);
            $table->integer('requisition_id')->unsigned();
            $table->foreign('requisition_id')->references('id')->on('requisitions');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('subcategory_id')->unsigned();
            $table->foreign('subcategory_id')->references('id')->on('types_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisitions');
    }
}
