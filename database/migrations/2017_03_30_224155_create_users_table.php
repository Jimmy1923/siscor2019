<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('avatar');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('active');
            $table->rememberToken();
            $table->timestamps();
            $table->string('api_token')->nullable();
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->integer('bussine_id')->unsigned();
            $table->foreign('bussine_id')->references('id')->on('bussines');
            $table->integer('area_id')->unsigned();
            $table->foreign('area_id')->references('id')->on('areas');
            $table->integer('selected_bussine_id')->unsigned()->nullable();
            $table->foreign('selected_bussine_id')->references('id')->on('bussines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
