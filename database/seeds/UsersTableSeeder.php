<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
        \DB::table('roles')->insert([
        	['name' => 'Administración'],
        	['name' => 'Validación'],
        	['name' => 'Preautorización'],
        	['name' => 'Dirección'],
            ['name' => 'Compra'],
            ['name' => 'Gerente']
        ]); 

        User::create([
        	'name'	=> 'Administrador',
            'lastname'  => 'Siscor',
        	'email'	=> 'admin@test.com',
        	'password' => bcrypt('12345678'),
            'address'   => '',
            'phone' => '',
            'avatar' => '/images/user.png',
            'active' => 0,
            'role_id' => 1,
            'bussine_id' => 1,
            'area_id' => 1
        	]);
    }
}
