<?php

use Illuminate\Database\Seeder;

class BussinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('bussines')->insert([
        	['name' => 'A DIARIO TABASCO', 'company_id' => '1'],
        	['name' => 'PROYECTOS ESPECIALES', 'company_id' => '1'],
        	['name' => 'DIGIT PHONE', 'company_id' => '2'],
        	['name' => 'PROYECTOS ESPECIALES', 'company_id' => '2'],
        	['name' => 'DISTRIBUIDORA DE TOLDOS Y LONAS', 'company_id' => '3'],
        	['name' => 'PROYECTOS ESPECIALES', 'company_id' => '3'],
        	['name' => 'COLCHON CITY', 'company_id' => '4'],
        	['name' => 'BODEGANGA', 'company_id' => '4'],
        	['name' => 'PROYECTOS ESPECIALES', 'company_id' => '4'],
        	['name' => 'TULIPANES', 'company_id' => '5'],
        	['name' => 'PLEGARIAS', 'company_id' => '5'],
        	['name' => 'PROYECTOS ESPECIALES', 'company_id' => '5'],
        	['name' => 'TALLER FORZA', 'company_id' => '6'],
        	['name' => 'PROYECTOS ESPECIALES', 'company_id' => '6'],
        	['name' => 'MARIA GARRIDO', 'company_id' => '7'],
        	['name' => 'NEW GLOBAL', 'company_id' => '7'],
        	['name' => 'PROYECTOS ESPECIALES', 'company_id' => '7'],
        	['name' => 'CORPORATIVO', 'company_id' => '8'],
        	['name' => 'PROYECTOS ESPECIALES', 'company_id' => '8'],
        	['name' => 'BIOMEDINT', 'company_id' => '9'],
        	['name' => 'PROYECTOS ESPECIALES', 'company_id' => '9'],
        ]);

        \DB::table('areas')->insert([
            ['name' => 'Administración', 'bussine_id' => '1'],
            
        ]);
    }
}
