<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->insert([
        	['name' => 'Gastos'],
            ['name' => 'Compras'],
            ['name' => 'Activo Fijo'],
        ]);

        \DB::table('types_categories')->insert([
            ['name' => 'Renta', 'category_id' => '1'],
            ['name' => 'Energía Eléctrica', 'category_id' => '1'],
            ['name' => 'Combustible', 'category_id' => '1'],
            ['name' => 'Teléfono Fijo', 'category_id' => '1'],
            ['name' => 'Telefonía Celular', 'category_id' => '1'],
            ['name' => 'Gas', 'category_id' => '1'],
            ['name' => 'Impuestos y Derechos', 'category_id' => '1'],
            ['name' => 'Honorarios por asesoría fiscal', 'category_id' => '1'],
            ['name' => 'Honorarios por asesoría Legal', 'category_id' => '1'],
            ['name' => 'Honorarios por serv. Administrativo', 'category_id' => '1'],
            ['name' => 'Comisión por ventas', 'category_id' => '1'],
            ['name' => 'Mensajería y paquetería', 'category_id' => '1'],
            ['name' => 'Fletes', 'category_id' => '1'],
            ['name' => 'Art. Limpieza', 'category_id' => '1'],
            ['name' => 'Papelería y art. de oficina', 'category_id' => '1'],
            ['name' => 'Mant. equipo de cómputo', 'category_id' => '1'],
            ['name' => 'Mant. equipo de transporte', 'category_id' => '1'],
            ['name' => 'Mant. maq. y equipo', 'category_id' => '1'],
            ['name' => 'Mantenimiento General', 'category_id' => '1'],
            ['name' => 'Uniformes', 'category_id' => '1'],
            ['name' => 'Fumigación', 'category_id' => '1'],
            ['name' => 'Seguros y finanzas', 'category_id' => '1'],
            ['name' => 'Equipo y accesorios', 'category_id' => '1'],
            ['name' => 'Agua', 'category_id' => '1'],
            ['name' => 'Publicidad y propaganda', 'category_id' => '1'],
            ['name' => 'Papel para impresión', 'category_id' => '1'],
            ['name' => 'Placas', 'category_id' => '1'],
            ['name' => 'Tintas', 'category_id' => '1'],
            ['name' => 'Químicos', 'category_id' => '1'],
            ['name' => 'Herramientas y equipos de mano', 'category_id' => '1'],
            ['name' => 'Art. médicos y desechables', 'category_id' => '1'],
            ['name' => 'Botiquín', 'category_id' => '1'],
            ['name' => 'Recolección de basura', 'category_id' => '1'],
            ['name' => 'Art. de cocina', 'category_id' => '1'],
            ['name' => 'Colchones', 'category_id' => '2'],
            ['name' => 'Tés', 'category_id' => '2'],
            ['name' => 'Transferones', 'category_id' => '2'],
            ['name' => 'Equipo de computo', 'category_id' => '3'],
            ['name' => 'Equipo de transporte', 'category_id' => '3'],
            ['name' => 'Mobiliario y equipo de oficina', 'category_id' => '3'],
            ['name' => 'Maquinaria y equipo', 'category_id' => '3'],
            ['name' => 'Insumos', 'category_id' => '3'],
            ['name' => 'Lonas', 'category_id' => '3'],
        ]);
    }
}
