
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/requisiciones', function () {
    return view('/home');
});

Route::get('/requisiciones', 'RequisitionController@index');

// inyección de dependencias en Compañias
Route::bind('company', function($company){
	return App\Company::find($company);
});

// inyección de dependencias en Empresas
Route::bind('bussine', function($bussine){
	return App\Bussine::find($bussine);
});

// inyección de dependencias en areas
Route::bind('area', function($area){
	return App\Area::find($area);
});

// inyección de dependencias en proveedores
Route::bind('provider', function($provider){
	return App\Provider::find($provider);
});

Route::get('get_unread', function() {
	return Auth::user()->unreadNotifications;
});

Route::get('/notifications', 'HomeController@notifications');

Auth::routes();
Route::group(['middleware' => 'auth'], function (){

	Route::get('/home', 'HomeController@index');
	Route::resource('/company', 'CompanyController');
	Route::resource('product', 'ProductController');
	Route::get('product/subcategory/{id}', 'ProductController@getCategory');
	Route::resource('/bussine', 'BussineController');
	Route::resource('/area', 'AreaController');
	Route::resource('/provider', 'ProviderController');
	//add routes for users crud
	Route::resource('users', 'UserController');
	Route::get('/profile', 'UserController@getProfile');
	Route::post('/profile', 'UserController@postProfile'); 
	//add routes for categories crud
	Route::resource('categories', 'CategoryController');
	//add routes for subcategories crud
	Route::resource('subcategories', 'SubCategoryController');
	//add routes for requisitions crud
	Route::resource('requisitions', 'RequisitionController');
	Route::get('/api/requisitions/{id}/providers', 'RequisitionController@getProviders');
	Route::get('bussines/{id}', 'RequisitionController@getBussine');
	Route::get('requisitions/subcategory/{id}', 'RequisitionController@getCategory');
	//add routes for status change
	Route::get('requisitions/{id}/validate', 'RequisitionController@validacion');
	Route::get('requisitions/{id}/preauthorize', 'RequisitionController@preauthorize');
	Route::get('requisitions/{id}/authorizar', 'RequisitionController@authorization');
	Route::get('requisitions/{id}/process', 'RequisitionController@process');
	Route::get('requisitions/{id}/payment', 'RequisitionController@payment');
	Route::get('requisitions/{id}/rejected', 'RequisitionController@rejected');
	Route::get('requisitions/{id}/finalizadas', 'RequisitionController@finalizadas');
	Route::post('requisitions/uploadticket', [
		'uses' => 'RequisitionController@uploadticket',
		'as' 	=> 'requisitions.uploadticket'
		]);
	Route::post('requisitions/uploadvoucher', [
		'uses' => 'RequisitionController@uploadvoucher',
		'as' 	=> 'requisitions.uploadvoucher'
		]);
	//routes for comments
	Route::get('/api/requisitions/{requisition}/comments', 'CommentController@index');
	Route::post('/api/requisitions/{requisition}/comment', 'CommentController@store');
	//add routes for select bssines
	Route::get('select/bussine/{id}', 'RequisitionController@selectBussine');
	//add routes for export excel report
	Route::get('/reports-general', 'ExcelController@index');
	Route::post('/search-reports', [
		'uses' => 'ExcelController@search',
		'as' => 'searchreport.store'
		]);
	//reports requisition print
	Route::get('/export-requisition/{id}/print', 'ExcelController@exportRequisition');
	//activities
	Route::get('/activities', 'ActivityController@viewActivity');
});