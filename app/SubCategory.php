<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = "types_categories";
    protected $fillable = [
        'name','category_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    
    public static function subcategory($id){
        return SubCategory::where('category_id','=',$id)->get();
    }
}
