<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "areas";
    protected $fillable = [
        'name', 'bussine_id',
    ];

      public function bussine()
    {
    	return $this->belongsTo(Bussine::class);
    }

      public function user()
    {
    	return $this->belongsTo(User::class);
    }

}
