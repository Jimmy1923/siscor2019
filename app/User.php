<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'name', 'email', 'password','lastname','address','phone','role','role_id','bussine_id','area_id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function bussine()
    {
        return $this->belongsTo(Bussine::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function requisition()
    {
        return $this->hasMany(Requisition::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function getFullNameAttribute()
    {
        return $this->name . " " . $this->lastname;
    }
    public function getBussineNameAttribute()
    {
        if ($this->bussine_id) {
            return $this->bussine->name;
        }
    }
    public function getAreaNameAttribute()
    {
        if ($this->area_id) {
            return $this->area->name;
        }
    }

    public function getTipoUserAttribute()
    {
        switch ($this->role_id) {
            case '1':
                return 'Administrador';
                break;
            case '2':
                return 'Validación';
                break;
            case '3':
                return 'Preautorizar';
                break;
            case '4':
                return 'Dirección';
                break;
            case '5':
                return 'Compra';
            default:
                return 'Gerente';
        }
    }
    public function getIsAdminAttribute()
    {
        return $this->role_id == 1;
    }
    public function getIsValidationAttribute()
    {
        return $this->role_id == 2;
    }
    public function getIsProcessAttribute()
    {
        return $this->role_id == 2;
    }
    public function getIsPreathorizeAttribute()
    {
        return $this->role_id == 3;
    }
    public function getIsAuthorizeAttribute()
    {
        return $this->role_id == 4;
    }
    public function getIsPaymentAttribute()
    {
        return $this->role_id == 5;
    }
    public function getIsGerenteAttribute()
    {
        return $this->role_id == 6;
    }

    public function activity()
    {
        return $this->hasMany('App\Activity')
            ->with(['user', 'subject'])
            ->latest();
    }
    /**
     * Record new activity for the user.
     *
     * @param  string $name
     * @param  mixed  $related
     * @throws \Exception
     * @return void
     */
    public function recordActivity($name, $related)
    {
        if (! method_exists($related, 'recordActivity')) {
            throw new \Exception('..');
        }
        return $related->recordActivity($name);
    }

    
}
