<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewRequisition extends Notification implements ShouldQueue
{
    use Queueable;

    public $requisition;
  

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($requisition)
    {
        $this->requisition = $requisition;
        
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Se ha generado una nueva requisición ' . $this->requisition->title)
                    ->action('Ver requisición', route('requisitions.show', $this->requisition->id))
                    ->line('Gracias por usar nuestra aplicación!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => $this->requisition->title,
            'message' => $this->requisition->title . ' nueva requisición en siscor.'
        ];
    }
}
