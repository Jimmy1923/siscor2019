<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Bussine extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "bussines";
    protected $fillable = [
        'name', 'company_id',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function requisition()
    {
        return $this->hasMany(Requisition::class);
    }
    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function area()
    {
        return $this->hasMany(Area::class);
    }

    public static function bussine($id){
        return Bussine::where('company_id','=',$id)->get();
    }

}
