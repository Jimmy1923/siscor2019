<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use Session;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $search = trim($request->get('searchText'));
            $categories = Category::where('name', 'LIKE', '%'.$search.'%')
                            ->orderBy('id', 'ASC')
                            ->paginate(10);
        }
        return view('categories.index', compact('categories','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subcategories(Request $request)
    {
        if ($request) {
            $search = trim($request->get('searchText'));
            $categories = SubCategory::where('name', 'LIKE', '%'.$search.'%')
                            ->orderBy('id', 'ASC')
                            ->paginate(10);
        }
        return view('categories.index', compact('categories','search'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required|min:2',
            ]
            );
        $category = new Category($request->all());
        $category->save();

        Session::flash('success', 'La categoria se ha creado exitosamente');

        return redirect('/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return response()->json(
                $category->toArray()
            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->fill($request->all());
        $category->save();

        return response()->json([
                'message' => 'listo'
            ]);
        
    }

    public function getUpdate(Request $request)
    {
        if ($request->ajax()) {
            $category = Category::findOrFail($request->id);
            return response($category);
        }
    }

    public function newUpdate(Request $request)
    {
        if ($request->ajax()) {
            $category = Category::findOrFail($request->id);
            $category->name = $request->name;
            $category->save();
            return response($category);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
