<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUser;
use App\Area;
use App\Bussine;
use App\Role;
use App\User;
use Session;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $search = trim($request->get('searchText'));
            $users = User::where('name', 'LIKE', '%'.$search.'%')
                            ->orWhere('email', 'LIKE', '%'.$search.'%')
                            ->orderBy('id', 'ASC')
                            ->paginate(10);
        }
        return view('users.index', compact('users','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $bussines = Bussine::all();
        $areas = Area::all();
        return view('users.create',compact('roles','bussines','areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = new User($request->all());
        $pass = $request->input('password');
        $user->password = bcrypt($request->input('password'));
        $user->avatar = '/images/user.png';
        $user->active = 0;
        if ($request->role_id == 6) {
            $user->selected_bussine_id = $request->bussine_id;
        }
        $user->save();
        Mail::to($user->email)->send(new NewUser($user, $pass));
        Session::flash('success', 'El usuario se ha creado correctamente');
        return redirect('/users');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        $bussines = Bussine::all();
        $areas = Area::all();
        return view('users.edit', compact('user','roles','bussines','areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'name' => 'required|min:2',
                'email' => 'required|email',
                'role_id' => 'required|numeric',
                'bussine_id' => 'required|numeric',
                'area_id'   => 'required|numeric'
            ]
            );
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();
        Session::flash('success', 'Usuario modificado exitosamente');
        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->active = 1;
        $user->save();
        Session::flash('success', 'Usuario desactivado exitosamente');
        return redirect('/users');
    }

    public function getProfile()
    {
        return view('users.profile');
    }
    public function postProfile(Request $request)
    {
        $user = $request->user();
        if($request->file('avatar'))
        {
            $file = $request->file('avatar');
            $name = 'perfil_'. time() . '.' .$file->getClientOriginalExtension();
            $path =  '/images/perfil/';
            $file->move($path, $name);   
        }
        if ($request->get('password') == !null && $request->file('avatar') == !null) {
            
                if(!Hash::check($request->get('current_password'), $user->password))
                {
                return redirect()->back()->withErrors([
                    'current_password' => 'La contreña no es validad'
                    ]);
                }
                $this->validate($request, [
                    'password' => 'required|confirmed',
                    'password_confirmation' => 'required'
                ]);
                $user->where('email', '=', $user->email)->update(['avatar' => '/images/perfil/'.$name, 'password' => bcrypt($request->get('password'))]);
                $user->save();
                return redirect('/users')->with('notification', 'Los cambios se guardaron exitosamente');
        } elseif ($request->get('password') == !null) {
            if(!Hash::check($request->get('current_password'), $user->password))
                {
                return redirect()->back()->withErrors([
                    'current_password' => 'La contreña no es validad'
                    ]);
                }
                $this->validate($request, [
                    'password' => 'required|confirmed',
                    'password_confirmation' => 'required'
                ]);
                $user->where('email', '=', $user->email)->update(['password' => bcrypt($request->get('password'))]);
                $user->save();
                return redirect('/users')->with('notification', 'Los cambios se guardaron exitosamente');
        }
        

        $user->where('email', '=', $user->email)->update(['avatar' => '/images/perfil/'.$name]);
        $user->save();
        Session::flash('success', 'Usuario modificado exitosamente');

        return redirect('/users');
    }
}
