<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SaveBussineRequest;
use App\Bussine;
use App\Company;

class BussineController extends Controller
{
    public function index()
    {
	    $bussines = Bussine::orderBy('id', 'desc')->paginate(5);
	    return view('bussines.index', compact('bussines'));
    }

    public function create()
    {
        $companies = Company::orderBy('id', 'desc')->pluck('name', 'id');
        return view('bussines.create', compact('companies'));

    }    

    public function store(request $request)
    {
        $data = [
            'name'          => $request->get('name'),
            'company_id'   => $request->get('company_id')
        ];

        $bussine = Bussine::create($data);
        $message = $bussine ? 'Linea de negocio agregada correctamente!' : 'La linea de negocio NO pudo agregarse!';      
        return redirect()->route('bussine.index')->with('message', $message);
    }

        public function show(Bussine $bussine)
    {
        return $bussine;
    }

    public function edit(Bussine $bussine)
    {
        $companies = Company::orderBy('id', 'desc')->pluck('name', 'id'); 

        return view('bussines.edit', compact('bussine','companies'));
    }

    public function update(Request $request, Bussine $bussine)
    {
        $bussine->fill($request->all());
        $updated = $bussine->save();
        
        $message = $updated ? 'La linea de negocio se he actualizado correctamente!' : 'La linea de negocio NO pudo actualizarse!';
        
        return redirect()->route('bussine.index')->with('message', $message);
    }

    public function destroy(Bussine $bussine)
    {
        $deleted = $bussine->delete();
        
        $message = $deleted ? 'La linea de negocio se ha eliminado correctamente!' : 'La linea de negocio NO se pude eliminarse!';
        
        return redirect()->route('bussine.index')->with('message', $message);
    }
}
