<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provider;

class ProviderController extends Controller
{
    public function index(Request $request)
    {
        if ($request) {
            $search = trim($request->get('searchText'));
            $providers = Provider::where('name', 'LIKE', '%'.$search.'%')
                            ->orderBy('id', 'ASC')
                            ->paginate(10);
        }

    	return view('providers.index', compact('providers','search'));
	}

	public function create()
    {
    	return view('providers.create');
	}

	public function store(Request $request)
    {		
		$providers = Provider::create([
			'name' => $request->get('name'),
    		'address' => $request->get('address'),
    		'rfc' => $request->get('rfc'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email')
    		]);
    $message = $providers ? 'Proveedores Agregada Correctamente' : 'Proveedores no puede agregarse';
    return back()->with('message',$message);
	}

	public function show(Provider $provider)
    {		
    	return $provider;
	}

	public function edit(Provider $provider)
    {		
    	return view('providers.edit', compact('provider'));
	}

    public function update(Request $request, Provider $provider)
    {
        $provider->fill($request->all());
                $updated = $provider->save();
        
        $message = $updated ? 'Los Proveedores se he actualizado correctamente!' : 'Los Proveedores NO pudo actualizarse!';
        
        return redirect()->route('provider.index')->with('message', $message);
    }

    public function destroy(Provider $provider)
    {
        $deleted = $provider->delete();
        
        $message = $deleted ? 'Los Proveedores se ha eliminado correctamente!' : 'Los Proveedores NO se pude eliminarse!';
        
        return redirect()->route('provider.index')->with('message', $message);
    }
}
