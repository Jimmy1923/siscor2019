<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompanyController extends Controller
{
    public function index(Request $request)
    {
        if ($request) {
            $search = trim($request->get('searchText'));
            $companies = Company::where('name', 'LIKE', '%'.$search.'%')
                            ->orderBy('id', 'ASC')
                            ->paginate(30);
        }
        
    	// $companies = Company::all();
    	return view('companies.index', compact('companies'));
	}

	public function create()
    {
    	return view('companies.create');
	}

	public function store(Request $request)
    {		
		$companie = Company::create([
			'name' => $request->get('name'),
    		'address' => $request->get('address'),
    		'rfc' => $request->get('rfc')
    		]);
    $message = $companie ? 'Compañia Agregada Correctamente' : 'La compañia no pudo agregarse';
    return redirect()->route('company.index')->with('message',$message);
	}

	public function show(Company $company)
    {		
    	return $company;
	}

	public function edit(Company $company)
    {		
    	return view('companies.edit', compact('company'));
	}

    public function update(Request $request, Company $company)
    {
        $company->fill($request->all());
                $updated = $company->save();
        
        $message = $updated ? 'La Compañia se he actualizado correctamente!' : 'La Compañia NO pudo actualizarse!';
        
        return redirect()->route('company.index')->with('message', $message);
    }

    public function destroy(Company $company)
    {
        $deleted = $company->delete();
        
        $message = $deleted ? 'La Compañia se ha eliminado correctamente!' : 'La Compañia NO se pude eliminarse!';
        
        return redirect()->route('company.index')->with('message', $message);
    }
}