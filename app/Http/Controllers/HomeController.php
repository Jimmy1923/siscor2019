<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requisition;
use App\Company;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $user = auth()->user();
        if ($user->is_gerente) {
            $companies = Company::all();
            $validations = Requisition::where('user_id', $user->id)->where('status', '=', 0)->get();
            $enpreauthorizes = Requisition::where('user_id', $user->id)->where('status', '=', 1)->get();
            $preauthorizes = Requisition::where('user_id', $user->id)->where('status', '=', 2)->get();
            $authorizes = Requisition::where('user_id', $user->id)->where('status', '=', 3)->get();
            $enpayments = Requisition::where('user_id', $user->id)->where('status', '=', 4)->get();
            $payments = Requisition::where('user_id', $user->id)->where('status', '=', 5)->get();
            $rejecteds = Requisition::where('user_id', $user->id)->where('status', '=', 6)->get();
            $finalizadas = Requisition::where('user_id', $user->id)->where('status', '=', 7)->get();
            return view('home', compact('validations','enpreauthorizes','preauthorizes','authorizes','enpayments','payments','rejecteds','finalizadas', 'companies'));
        }
        $companies = Company::all();
        if (26!=$user->selected_bussine_id) 
        {
            $validations = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 0)->get();
            $enpreauthorizes = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 1)->get();
            $preauthorizes = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 2)->get();
            $authorizes = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 3)->get();
            $enpayments = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 4)->get();
            $payments = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 5)->get();
            $rejecteds = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 6)->get();
            $finalizadas = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 7)->get();    
        } 
        elseif (26==$user->selected_bussine_id) 
        {
            $validations = Requisition::where('status', '=', 0)->get();
            $enpreauthorizes = Requisition::where('status', '=', 1)->get();
            $preauthorizes = Requisition::where('status', '=', 2)->get();
            $authorizes = Requisition::where('status', '=', 3)->get();
            $enpayments = Requisition::where('status', '=', 4)->get();
            $payments = Requisition::where('status', '=', 5)->get();
            $rejecteds = Requisition::where('status', '=', 6)->get();
            $finalizadas = Requisition::where('status', '=', 7)->get();
        }
        return view('home', compact('validations','enpreauthorizes','preauthorizes','authorizes','enpayments','payments','rejecteds','finalizadas', 'companies'));
    }

    public function notifications()
    {
        Auth::user()->unreadNotifications->markAsRead();

        return view('notifications')->with('nots', Auth::user()->notifications);
    }

}
