<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bussine;
use App\Area;

class AreaController extends Controller
{
    public function index(Request $request)
    {
	   if ($request) {
            $search = trim($request->get('searchText'));
            $areas = Area::where('name', 'LIKE', '%'.$search.'%')
                            ->orderBy('id', 'ASC')
                            ->paginate(10);
        
        }
        $bussines = Bussine::all();
        return view('areas.index', compact('areas','search', 'bussines'));


    }

    public function create()
    {
        $bussines = Bussine::findOrFail($id);
        return response()->json(
                $areas->toArray()
            );
    }

    // public function create()
    // {
    //     $bussines = Bussine::orderBy('id', 'desc')->pluck('name', 'id');
    //     return view('areas.create', compact('bussines'));
    // }    

    public function store(request $request)
    {
        $data = [
            'name'          => $request->get('name'),
            'bussine_id'   => $request->get('bussine_id')
        ];
        $Areas = Area::create($data);
        $message = $Areas ? 'La area se ha agregada correctamente!' : 'La area NO pudo agregarse!';      
        return redirect()->route('area.index')->with('message', $message);
    }

    public function edit(Area $area)
    {
        $bussines = Bussine::orderBy('id', 'desc')->pluck('name', 'id'); 
        return view('areas.edit', compact('bussines','area'));
    }

    public function update(Request $request, Area $area)
    {
        $area->fill($request->all());
        $updated = $area->save();
        
        $message = $updated ? 'La area se he actualizado correctamente!' : 'La area NO pudo actualizarse!';
        
        return redirect()->route('area.index')->with('message', $message);
    }

    public function destroy(Area $area)
    {
        $deleted = $area->delete();
        
        $message = $deleted ? 'La Area se ha eliminado correctamente!' : 'La Area NO se pude eliminarse!';
        return redirect()->route('area.index')->with('message', $message);
    }

}
