<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequisitionRequest;
use App\SubCategory;
use App\Company;
use App\Bussine;
use App\Category;
use App\Provider;
use App\Product;
use App\DetailRequisition;
use App\Requisition;
use App\User;
use DB;
use Session;
use App\Notifications\NewRequisition;
use Carbon\Carbon;
class RequisitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if ($user->is_gerente) {
            $validations = Requisition::where('user_id', $user->id)->where('status', '=', 0)->get();
            $enpreauthorizes = Requisition::where('user_id', $user->id)->where('status', '=', 1)->get();
            $preauthorizes = Requisition::where('user_id', $user->id)->where('status', '=', 2)->get();
            $authorizes = Requisition::where('user_id', $user->id)->where('status', '=', 3)->get();
            $enpayments = Requisition::where('user_id', $user->id)->where('status', '=', 4)->get();
            $payments = Requisition::where('user_id', $user->id)->where('status', '=', 5)->get();
            $rejecteds = Requisition::where('user_id', $user->id)->where('status', '=', 6)->get();
            $finalizadas = Requisition::where('user_id', $user->id)->where('status', '=', 7)->get();
            return view('requisitions.index', compact('validations','enpreauthorizes','preauthorizes','authorizes','enpayments','payments','rejecteds','finalizadas', 'companies'));
        }
        $companies = Company::all();
        if (26!=$user->selected_bussine_id) 
        {
            $validations = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 0)->get();
            $enpreauthorizes = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 1)->get();
            $preauthorizes = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 2)->get();
            $authorizes = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 3)->get();
            $enpayments = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 4)->get();
            $payments = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 5)->get();
            $rejecteds = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 6)->get();
            $finalizadas = Requisition::where('bussine_id', $user->selected_bussine_id)->where('status', '=', 7)->get();    
        } 
        elseif (26==$user->selected_bussine_id) 
        {
            $validations = Requisition::where('status', '=', 0)->get();
            $enpreauthorizes = Requisition::where('status', '=', 1)->get();
            $preauthorizes = Requisition::where('status', '=', 2)->get();
            $authorizes = Requisition::where('status', '=', 3)->get();
            $enpayments = Requisition::where('status', '=', 4)->get();
            $payments = Requisition::where('status', '=', 5)->get();
            $rejecteds = Requisition::where('status', '=', 6)->get();
            $finalizadas = Requisition::where('status', '=', 7)->get();
        }
        return view('requisitions.index', compact('validations','enpreauthorizes','preauthorizes','authorizes','enpayments','payments','rejecteds','finalizadas', 'companies'));
    }

    public function getBussine(Request $request, $id){
        if($request->ajax()){
            $bussine = Bussine::bussine($id);
            return response()->json($bussine);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $providers = Provider::all();
        $categories = Category::all();
        $products = Product::all();
        $companies = Company::all();
         $user = auth()->user();
        return view('requisitions.create', compact('providers','categories','products','companies','user'));
    }

    public function getCategory(Request $request, $id){
        if($request->ajax()){
            $subcategory = SubCategory::subcategory($id);
            return response()->json($subcategory);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequisitionRequest $request)
    {
        try {
            DB::beginTransaction();
            $id = auth()->user()->id;
            $user = User::findOrFail($id);
            $folio = DB::table('requisitions')->select('folio')->count();
            $requisition = new Requisition($request->all());
            $requisition->folio = $folio++;
            $requisition->status = 0;
            if ($user->role_id==8) {
              $requisition->status=1;
            }
            $requisition->area_id = $user->area_id;
            if ($user->role_id == 7 || $user->bussine_id == 26) {
                $requisition->bussine_id = $request->get('list-of-bussines_ids');
            } elseif($user->role_id == 1|$user->role_id == 2|$user->role_id == 3|$user->role_id == 4|$user->role_id == 5|$user->role_id == 6 | $user->role_id == 8) {
                 $requisition->bussine_id = $user->bussine_id;
            }            
            $requisition->user_id = $user->id;
            //dd($requisition);
            $requisition->save();

            $idproduct = $request->get('product_id');
            $quantity = $request->get('quantity');
            $unity = $request->get('unity');
            $price = $request->get('price');
            $iva = $request->get('iva');
            $payment = $request->get('type_payment');
            $category_id = $request->get('category_id');
            $subcategory_id = $request->get('subcategory_id');
            $cont = 0;

            while ($cont < count($idproduct)) {
                $details = new DetailRequisition();
                $details->requisition_id = $requisition->id;
                $details->product_id = $idproduct[$cont];
                $details->quantity = $quantity[$cont];
                $details->unity = $unity[$cont];
                $details->price = $price[$cont];
                $details->iva = $iva[$cont];
                $details->type_payment=$payment;
                $details->category_id=$category_id;
                $details->subcategory_id=$subcategory_id;
                $details->save();
                $cont = $cont+1;
            }
            $path = public_path().'/requisiciones/';
            $files = $request->file('file');
            $requisition = Requisition::findOrFail($requisition->id);
           foreach ($files as $file) {
                $fileName = uniqid() . $file->getClientOriginalName();
                $file->move($path, $fileName);
                $archive = $requisition->archives()->create([
                    'archive' => $fileName,
                    'requisition_id' => $requisition->id,
                    'user_id'   => $user->id,
                    ]);
            }
            DB::commit();            
        } catch (Exception $e) {
            DB::rollback();
        } 
        //sending notifications, emails, broadcasting.
        $user = User::where('role_id', 2)->first();
        $user->notify(new NewRequisition($requisition));

        return $requisition;
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $requisition = Requisition::findOrFail($id);
        $dRequisition = DetailRequisition::where('requisition_id', $id)->first();

        $details = DB::table('details_requisitions')
                    ->join('products','products.id', '=', 'details_requisitions.product_id')
                    ->select('products.name', 'details_requisitions.*')
                    ->where('details_requisitions.requisition_id', $id)
                    ->get();

        return view('requisitions.show', compact('requisition', 'details','dRequisition'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validacion($id)
    {
        $requisition = Requisition::findOrFail($id);
        $requisition->status = 1;
        $requisition->validation_id = auth()->user()->id;
        $requisition->save();
        //sending notifications, emails, broadcasting.
        $user = User::where('role_id', 3)->first();
        $user->notify(new NewRequisition($requisition));
        Session::flash('success', 'Requisición validada con éxito');

        //return $requisition;
        return redirect('/home');
    }
    public function preauthorize($id)
    {
        $requisition = Requisition::findOrFail($id);
        $requisition->status = 2;
        $requisition->preauthorize_id = auth()->user()->id;
        $requisition->save();
        //sending notifications, emails, broadcasting.
        $user = User::where('role_id', 4)->first();
        $user->notify(new NewRequisition($requisition));
        Session::flash('success', 'Requisición Pre-autorizada con éxito');
        return redirect('/home');
    }
    public function authorization($id)
    {
        $requisition = Requisition::findOrFail($id);
        $requisition->status = 3;
        $requisition->authorize_id = auth()->user()->id;
        $requisition->save();
        //sending notifications, emails, broadcasting.
        $user = User::where('role_id', 2)->first();
        $user->notify(new NewRequisition($requisition));
        Session::flash('success', 'Requisición Autorizada con éxito');
                return redirect('/home');

    }
    public function process($id)
    {
        $requisition = Requisition::findOrFail($id);
        $requisition->status = 4;
        $requisition->save();
        //sending notifications, emails, broadcasting.
        $user = User::where('role_id', 5)->first();
        $user->notify(new NewRequisition($requisition));
        Session::flash('success', 'Requisición en proceso de pago con éxito');
        return redirect('/home');

    }
    public function payment($id)
    {
        $requisition = Requisition::findOrFail($id);
        $requisition->status = 5;
        $requisition->payment_id = auth()->user()->id;
            $date = Carbon::now();
            $dates = $date->format('d-m-Y');
        $requisition->fechadepago = $dates;
        $requisition->save();
       //sending notifications, emails, broadcasting.
        $user = User::where('role_id', 6)->first();
        $user->notify(new NewRequisition($requisition));
        Session::flash('success', 'Requisición pagada con éxito');
        return redirect('/home');

    }
    public function rejected($id)
    {
        $requisition = Requisition::findOrFail($id);
        $requisition->status = 6;
        $requisition->rejected_id = auth()->user()->id;
        $requisition->save();
        //sending notifications, emails, broadcasting.
        $user = User::where('role_id', 6)->first();
        $user->notify(new NewRequisition($requisition));
        Session::flash('success', 'Requisición rechazada con éxito');
        return redirect('/home');

    }
    public function finalizadas($id)
    {
        $requisition = Requisition::findOrFail($id);
        $requisition->status = 7;
        $requisition->finish_id = auth()->user()->id;
        $requisition->save();
        //sending notifications, emails, broadcasting.
        $user = User::where('role_id', 2)->first();
        $user->notify(new NewRequisition($requisition));
        Session::flash('success', 'Requisición finalizada con éxito');
        return redirect('/home');

    }
 
    public function getProviders($id)
    {
        if (request()->ajax()) {
            return Provider::findOrFail($id);
        }
        return back();
    }

    public function selectBussine($id)
    {
        $user = auth()->user();
        $user->selected_bussine_id = $id;
        $user->save();
        return back();
    }
    public function uploadticket(Request $request)
    {
        $file = $request->file('file');
        $fileName = uniqid() . $file->getClientOriginalName();
        $file->move('requisiciones/tickets', $fileName);
        $requisition = Requisition::findOrFail($request->input('requisition_id'));
        $requisition->archive_payment = $fileName;
        $requisition->save();
        return $requisition;
    }
    public function uploadvoucher(Request $request)
    {
        $file = $request->file('file');
        $fileName = uniqid() . $file->getClientOriginalName();
        $file->move('requisiciones/vouchers', $fileName);
        $requisition = Requisition::findOrFail($request->input('requisition_id'));
        $requisition->archive_ticket = $fileName;
        $requisition->save();
        return $requisition;
    }
}
