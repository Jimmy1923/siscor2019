<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ActivityController extends Controller
{
	public function viewActivity()
	{
		return view('activities.show');
	}
    public function index()
    {
    	$activities = auth()->user()->activity()->latest()->get();
    	
    	return fractal($activities, new ActivityTransformer())->respond();
    }
}
