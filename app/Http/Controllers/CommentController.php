<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Requisition;
use App\Events\NewComment;
class CommentController extends Controller
{
 	public function index(Requisition $requisition)
 	{
 		return response()->json(
 				$requisition->comments()->with('user')->latest()->get()
 			);
 	}   

 	public function store(Requisition $requisition)
 	{
 		$comment = $requisition->comments()->create([
 				'comment' => request('comment'),
 				'user_id' => auth()->user()->id,
 				'requisition_id' => $requisition->id
 			]);
 		$comment = Comment::where('id', $comment->id)->with('user')->first();
		broadcast(new NewComment($comment))->toOthers();
		return $comment;
 		
 	}
}
