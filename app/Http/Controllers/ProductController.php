<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Category;
use App\SubCategory;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if ($request) {
            $search = trim($request->get('searchText'));
            $product = Products::where('name', 'LIKE', '%'.$search.'%')
                            ->orderBy('id', 'ASC')
                            ->paginate(10);
        }
    	return view('products.index', compact('product','search'));
	}

    public function getCategory(Request $request, $id){
        if($request->ajax()){
            $subcategory = SubCategory::subcategory($id);
            return response()->json($subcategory);
        }
    }

	public function store(Request $request)
    {	
        $this->validate($request, [
                'name' => 'required|min:2',
            ]
            );
        $product = new Products($request->all());
        $product->save();

        return back()->with('notification', 'El producto se ha creado exitosamente');
		
	}

	public function show($id)
    {		
    	//
	}

	public function edit($id)
    {		
    	$products = Products::findOrFail($id);
        return view('products.edit', compact('products'));
	}

    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'name' => 'required|min:2',
            ]
            );
        $product = Products::findOrFail($id);
        $product->fill($request->all());
        $product->save();

        return redirect('/product')->with('notification', 'El producto se ha modificado correctamente');
        
    }

    public function destroy($id)
    {
        
    }
}

