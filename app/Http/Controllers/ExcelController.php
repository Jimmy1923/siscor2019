<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Requisition;
use App\Company;
use App\Bussine;
use App\User;
use DB;
class ExcelController extends Controller
{
	public function index()
	{
		$companies = Company::all();
		return view('reports.index', compact('companies'));
	}
	public function search(Request $request)
	{
		
		if ($request->input('details') == 'on') {
			Excel::create('Requisiciones', function($excel) use($request){
				$bussine_id = $request->input('bussine');
				$created_at = $request->input('created_at');
				$finish_at =  $request->input('finish_at');
				$status = $request->input('status');
				if ($bussine_id == 26) {
					$requisitions = Requisition::select('requisitions.*','details.*','categories.name as categorias')
											->join('details_requisitions as details','details.requisition_id', 'requisitions.id')
											->join('categories','details.category_id','=','categories.id')
											->where('requisitions.status', $status)
											->whereBetween('requisitions.created_at', [$created_at, $finish_at])
											->get();
				} else {
					$requisitions = Requisition::select('requisitions.*','details.*','categories.name as categorias')
										->join('details_requisitions as details','details.requisition_id', 'requisitions.id')
										->join('categories','details.category_id','=','categories.id')
										->where('requisitions.bussine_id', $bussine_id)
										->where('requisitions.status', $status)
										->whereBetween('requisitions.created_at', [$created_at, $finish_at])
										->get();
					}
					// dd($requisitions);
			$excel->sheet('Requisiciones', function($sheet) use($requisitions){
				$sheet->row(1, [
					'Folio', 'Fecha de creacion','Proveedor','Concepto','Producto','precio','cantidad','iva','Subtotal','Total', 'Categoria','Forma de pago', 'Fecha de pago','Estatus','Empresa','Linea de Negocio'
					]);
				foreach ($requisitions as $index => $requi) {
					$sheet->row($index+2, [
						$requi->folio, $requi->created_at, $requi->provider->name, $requi->title, $requi->product->name,$requi->price, $requi->quantity, $requi->iva, ($requi->price*$requi->quantity)+$requi->iva, $requi->total, $requi->categorias, $requi->type_payment, $requi->fechadepago, $requi->state,$requi->bussine->company->name,$requi->bussine_name
						]);
				}
			});
		})->export('csv');
		}
		Excel::create('Requisiciones', function($excel) use($request){
			$bussine_id = $request->input('bussine');
			$created_at = $request->input('created_at');
			$finish_at =  $request->input('finish_at');
			$status = $request->input('status');
			if ($bussine_id == 26) {
				// $requisitions = Requisition::where('status', $status)
				// 							->whereBetween('created_at', [$created_at, $finish_at])
				// 							->get();

				$requisitions = Requisition::select('requisitions.folio','requisitions.created_at','requisitions.provider_id','requisitions.title','requisitions.date_programming',
				 				'requisitions.total','categories.name as categorias','details_requisitions.type_payment','requisitions.fechadepago','requisitions.status','requisitions.bussine_id')
								->join('details_requisitions','details_requisitions.requisition_id','=','requisitions.id')
								->join('categories','details_requisitions.category_id','=','categories.id')
								->where('requisitions.status', $status)
								->whereBetween('requisitions.created_at', [$created_at, $finish_at])
								->groupBy('requisitions.folio')
								->groupBy('requisitions.created_at')
								->groupBy('requisitions.provider_id')
								->groupBy('requisitions.title')
								->groupBy('requisitions.date_programming')
								->groupBy('requisitions.total')
								->groupBy('categorias')
								->groupBy('details_requisitions.type_payment')
								->groupBy('requisitions.fechadepago')
								->groupBy('requisitions.status')
								->groupBy('requisitions.bussine_id')
								->get();
			} else {
				// $requisitions = Requisition::where('bussine_id', $bussine_id)
				// 						->where('status', $status)
				// 						->whereBetween('created_at', [$created_at, $finish_at])
				// 						->get();	
				$requisitions = Requisition::select('requisitions.folio','requisitions.created_at','requisitions.provider_id','requisitions.title','requisitions.date_programming',
				 				'requisitions.total','categories.name as categorias','details_requisitions.type_payment','requisitions.fechadepago','requisitions.status','requisitions.bussine_id')
								->join('details_requisitions','details_requisitions.requisition_id','=','requisitions.id')
								->join('categories','details_requisitions.category_id','=','categories.id')
								->where('requisitions.status', $status)
								->where('requisitions.bussine_id', $bussine_id)
								->whereBetween('requisitions.created_at', [$created_at, $finish_at])
								->groupBy('requisitions.folio')
								->groupBy('requisitions.created_at')
								->groupBy('requisitions.provider_id')
								->groupBy('requisitions.title')
								->groupBy('requisitions.date_programming')
								->groupBy('requisitions.total')
								->groupBy('categorias')
								->groupBy('details_requisitions.type_payment')
								->groupBy('requisitions.fechadepago')
								->groupBy('requisitions.status')
								->groupBy('requisitions.bussine_id')
								->get();
								// dd($requisitions);			
				}
			$excel->sheet('Requisiciones', function($sheet) use($requisitions){
				$sheet->row(1, [
					'Folio', 'Fecha de creacion','Proveedor','Concepto','Fecha programada de pago','Total', 'Categorias','Forma de pago','Fecha de pago', 'Estatus','Empresa','Linea de Negocio'
					]);
				foreach ($requisitions as $index => $requi) {
					$sheet->row($index+2, [
						$requi->folio, $requi->created_at, $requi->provider->name, $requi->title, $requi->date_programming, $requi->total, $requi->categorias, $requi->type_payment,$requi->fechadepago,  $requi->state,$requi->bussine->company->name,$requi->bussine_name
						]);
				}
			});
		})->export('csv');
	}
	public function exportRequisition($id)
	{
		$date = date('Y-m-d');
		$requisition = Requisition::findOrFail($id);
        $details = DB::table('details_requisitions')
                    ->join('products','products.id', '=', 'details_requisitions.product_id')
                    ->select('products.name', 'details_requisitions.*')
                    ->where('details_requisitions.requisition_id', $id)
                    ->get();
        $view = \View::make('reports.pdf', compact('date','requisition','details'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('Requisición.pdf');

		
	}
    
}
