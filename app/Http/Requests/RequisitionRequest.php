<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequisitionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider_id' => 'required',
            'product_id'   => 'required',
            'title' => 'required|min:5',
            'date_programming' => 'required|date',
            'category_id'   => 'required',
            'subcategorie_id' => 'required',
            'type_payment'  => 'required',
            'unity' => 'required',
            'price' => 'required',
            'quantity' => 'required',
            'iva'   => 'required',
            
        ];
    }
}
