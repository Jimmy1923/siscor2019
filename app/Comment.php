<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "comments";
    protected $fillable = [
        'comment', 'requisition_id', 'user_id',
    ];

    public function bussine()
    {
        return $this->hasMany(Bussine::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function requisition()
    {
        return $this->belongsTo(Requisition::class);
    }

}
