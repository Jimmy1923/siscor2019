<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = "products";
    
    protected $fillable = [
        'name',
        'category_id',
        'subcategory_id', 
        'pago',
        'medida', 
        'precio',
        'cantidad',
        'subtotal',
        'requisitions_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

     public function subcategory()
    {
        return $this->belongsTo(SubCategory::class);
    }

    public function requisitions()
    {
        return $this->belongsTo(Requisition::class);
    }
}
