<?php
namespace App\Traists;

use App\Requisition;
use Illuminate\Http\Request;

trait Requisitionable
{
	public function add_requisition(Request $request)
	{
		try {
            DB::beginTransaction();
            $id = auth()->user()->id;
            $user = User::findOrFail($id);
            $folio = DB::table('requisitions')->select('folio')->count();
            $requisition = new Requisition($request->all());
            $requisition->folio = $folio++;
            $requisition->status = 0;
            $requisition->area_id = $user->area_id;
            $requisition->bussine_id = $user->bussine_id;
            $requisition->user_id = $user->id;
            $requisition->save();

            $idproduct = $request->get('product_id');
            $quantity = $request->get('quantity');
            $unity = $request->get('unity');
            $price = $request->get('price');
            $iva = $request->get('iva');
            $payment = $request->get('type_payment');
            $category_id = $request->get('category_id');
            $subcategory_id = $request->get('subcategory_id');
            $cont = 0;

            while ($cont < count($idproduct)) {
                $details = new DetailRequisition();
                $details->requisition_id = $requisition->id;
                $details->product_id = $idproduct[$cont];
                $details->quantity = $quantity[$cont];
                $details->unity = $unity[$cont];
                $details->price = $price[$cont];
                $details->iva = $iva[$cont];
                $details->type_payment=$payment;
                $details->category_id=$category_id;
                $details->subcategory_id=$subcategory_id;
                $details->save();
                $cont = $cont+1;
            }
            $path = public_path().'/requisiciones/';
            $files = $request->file('file');
            $requisition = Requisition::findOrFail($requisition->id);
           foreach ($files as $file) {
                $fileName = uniqid() . $file->getClientOriginalName();
                $file->move($path, $fileName);
                $archive = $requisition->archives()->create([
                    'archive' => $fileName,
                    'requisition_id' => $requisition->id,
                    'user_id'   => $user->id,
                    ]);
            }
            DB::commit();            
        } catch (Exception $e) {
            DB::rollback();
        }
        if ($requisition) {
        	return 1;
        }
        return 0;

	}
}