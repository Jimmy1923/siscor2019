<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="companies";
    protected $fillable = [
        'name', 'address', 'rfc',
    ];

    public function bussine()
    {
        return $this->hasMany('App\Bussine');
    }

        public function requisition()
    {
        return $this->hasMany(Requisition::class);
    }
}
