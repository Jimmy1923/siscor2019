<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchiveRequisition extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="archives_requisitions";
    protected $fillable = [
        'archive', 'requisition_id', 'user_id',
    ];

    public function requisition()
    {
    	return $this->belongsTo(Requisition::class);
    }

}
