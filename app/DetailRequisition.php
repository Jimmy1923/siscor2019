<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailRequisition extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "details_requisitions";
    protected $fillable = [
        'product_id', 'quantity', 'unity','iva','price','observations','riquisition_id','category_id', 'subcategory_id'
    ];

    public function requisition()
    {
        return $this->belongsTo(Requisition::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(SubCategory::class);
    }

    public function getCategoryNameAttribute()
    {
        if ($this->category_id) {
            return $this->category->name;
        }
        return;
    }
    public function getSubcategoryNameAttribute()
    {
        if ($this->subcategory_id) {
            return $this->subcategory->name;
        }
        return;
    }

    public function getTipoPagoAttribute()
    {
        switch ($this->type_payment) {
            case '0':
                return 'Efectivo';
                break;
            case '1':
                return 'Cheque';
                break;
	    case '2':
                return 'Transferencia';
                break;
            default:
                return 'Pago en linea';
                break;
        }
    }

}
