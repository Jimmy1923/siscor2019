<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisition extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "requisitions";
    protected $fillable = [
        'folio', 'title', 'description','status','archive_payment','archive_ticket','total','fechadepago','area_id','bussine_id','manager_id','user_id','provider_id','date_programming','validation_id','preathorize_id','authorize_id','payment_id','rejected_id','finish_id'
    ];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

     public function product()
    {
        return $this->belongsTo(Products::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function bussine()
    {
        return $this->belongsTo(Bussine::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function validation()
    {
        return $this->belongsTo(User::class, 'validation_id');
    }

    public function preauthorization()
    {
        return $this->belongsTo(User::class, 'preauthorize_id');
    }
    public function authorization()
    {
        return $this->belongsTo(User::class, 'authorize_id');
    }
    public function payment()
    {
        return $this->belongsTo(User::class, 'payment_id');
    }
    public function rejected()
    {
        return $this->belongsTo(User::class, 'rejected_id');
    }
    /*public function finish()
    {
        return $this->belongsTo(User::class, 'finish_id');
    }*/
    public function archives()
    {
        return $this->hasMany(ArchiveRequisition::class);
    }
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
    public function detallerequisition()
    {
        return $this->belongsTo(DetailRequisition::class);
    }

    public function archiveRequisition()
    {
        return $this->hasMany(ArchiveRequisition::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getStateAttribute()
    {
        switch ($this->status) {
            case '0':
                return 'En validación';
                break;
            case '1':
                return 'En espera pre-autorización';
                break;
            case '2':
                return 'Pre-autorizada';
                break;
            case '3':
                return 'Autorizada';
                break;
            case '4':
                return 'Proceso de pago';
                break;
            case '5':
                return 'Pagada';
                break;
            case '6':
                return 'Rechazada';
                break;
            default:
                return 'Finalizada';
        }
    }

    public function getBussineNameAttribute()
    {
        if ($this->bussine_id) {
            return $this->bussine->name;
        }
        return;
    }
    public function getCompanyNameAttribute()
    {
        if ($this->company_id) {
            return $this->company->name;
        }
        return;
    }
    public function getUserNameAttribute()
    {
        if ($this->user_id) {
            return $this->user->name . " " . $this->user->lastname;
        }
        return;
    }
    public function getAreaNameAttribute()
    {
        if ($this->area_id) {
            return $this->area->name;
        }
        return;
    }
    public function getProviderNameAttribute()
    {
        if ($this->provider_id) {
            return $this->provider->name;
        }
        return;
    }
    public function getProviderAddressAttribute()
    {
        if ($this->provider_id) {
            return $this->provider->address;
        }
        return;
    }
    public function getProviderRfcAttribute()
    {
        if ($this->provider_id) {
            return $this->provider->rfc;
        }
        return;
    }
    public function getValidationNameAttribute()
    {
        if ($this->validation) {
            return $this->validation->name . " ". $this->validation->lastname;
        }
        return;
    }
    
    public function getpreauthorizeNameAttribute()
    {
        if ($this->preauthorization) {
            return $this->preauthorization->name . " ". $this->preauthorization->lastname;
        }
        return;
    }
    public function getauthorizeNameAttribute()
    {
        if ($this->authorization) {
            return $this->authorization->name . " ". $this->authorization->lastname;
        }
        return;
    }
    public function getPaymentNameAttribute()
    {
        if ($this->payment) {
            return $this->payment->name . " ". $this->payment->lastname;
        }
        return;
    }
    public function getRejectedNameAttribute()
    {
        if ($this->rejected) {
            return $this->rejected->name . " ". $this->rejected->name;
        }
        return;
    }

}
