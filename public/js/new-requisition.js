$(function() {
	$("#provider_id").on('change', onSelectProviderChange);
});

function onSelectProviderChange() {
	var provider_id = $(this).val();
	$.get('/api/requisitions/'+provider_id+'/providers', function (data){
		$('#address').val(data.address);
		$("#rfc").val(data.rfc);
		$("#phone").val(data.phone);
		$("#email").val(data.email);
	});
}

$(document).ready(function(){
	$("#btn_add").click(function(){
		agregar();
	});
});
	
	var cont = 0;
	total = 0;
	subtotal=[];
	$("#save").hide();
	$("#provider_id").change(showValues);

	function showValues() {
		dataProduct = document.getElementById('product_id').value.split('_');
		$("#price").val(dataProduct[1]);
	}

	function agregar() {
		dataProduct = document.getElementById('product_id').value.split('_');
		$("#price").val(dataProduct[1]);
		product_id=dataProduct[0];
		product=$("#product_id option:selected").text();
		category=$("#category_id option:selected").text();
		subcategorie=$("#subcategory_id option:selected").text();
		quantity=$("#quantity").val();
		unity=$("#unity option:selected").text();
		price=$("#price_product").val();
		type_payment=$("#type_payment option:selected").text();
		iva=parseFloat($("#iva").val());			
		if (product_id!="" && quantity!="" && quantity > 0  && price!="") 
		{
			cal_iva = iva * (quantity*price);
			subtotal[cont] =cal_iva + (quantity*price);
			total=total + subtotal[cont];
			var fila = '<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-raised btn-warning btn-xs" onclick="eliminar('+cont+');">x</button></td><td><input type="hidden" name="product_id[]" value="'+product_id+'">'+product+'</td><td><input type="text" name="subcategorie_id[]" value='+subcategorie+'"></td><td><input type="text" name="type_payment" value="'+type_payment+'"></td><td><input type="text" name="unity[]" class="field" value="'+unity+'"></td><td><input type="number" name="price[]" class="field" value="'+price+'"></td><td><input type="number" name="quantity[]" class="field" value="'+quantity+'"></td><td><input type="text" name="iva[]" class="field" value="'+cal_iva+'"></td><td class="field">'+subtotal[cont]+'</td></tr>';
			cont++;
			evaluate();
			clean();
			$('#total').html("$ " + total);
			$('#total_re').val(total);
			$('#details').append(fila);
		}else {
			alert('Error al ingresar el detalle de la requisición, revise los datos del producto');
		}

	}

	function clean() {
		$("#quantity").val("");
		$("#iva").val("");
		$("#price_product").val("");
	}

	function evaluate() {
		if (total>0) {
			$("#save").show();

		}else {
			$("#save").hide();
		}
	}

	function eliminar(index) {
		total = total-subtotal[index];
		$("#total").html("$ " +total);
		$("#total_sale").val(total);
		$("#fila" + index).remove();
		evaluate();
	}
