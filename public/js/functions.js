//script initialize material
$.material.init();




//function for edit categories
function mostrar(btn) {
	var id = btn.value;
	$.get('/categories/'+id+'/edit', function(data) {
		$('#name').val(data.name);
		$("#id").val(data.id);
	});
}

$("#actualizar").click(function(e){
	e.preventDefault();
	var value = $("#id").val();
	var dato = $("#name").val();
	var token = $("#token").val();
      
	$.ajax({
		url: '/categories/'+value+'',
		headers: {'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data:{name: dato},
		success: function() {
                $('#edit-category').modal('toggle');
                $('#msj-success').fadeIn();
            }
	});
});

function mostrarSub(btn) {
	var id = btn.value;
	$.get('/subcategories/'+id+'/edit', function(data) {
		$('#name').val(data.name);
		$("#id").val(data.id);
	});
}

$("#actualizar-sub").click(function(){
	var value = $("#id").val();
	var dato = $("#name").val();
	var category_id = $('#category_selected').val();
	var token = $("#token").val();
	$.ajax({
		url: '/subcategories/'+value+'',
		headers: {'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data:{name: dato, category_id: category_id},
		success: function() {
                $('#edit-sub').modal('toggle');
                $('#msj-success').fadeIn();
            }
	});
});

$("#category_id").change(function(event){
    $.get(`subcategory/${event.target.value}`, function(response, category){
        $("#subcategory_id").empty();
        response.forEach(element => {
            $("#subcategory_id").append(`<option value=${element.id}> ${element.name} </option>`);
        });
    });
});

$(function() {
	$("#company_ids").on('change', onSelectBussineChanges);
});

function onSelectBussineChanges() {
	var bussine_id = $(this).val();
	if (! bussine_id) {
		$('#list-of-bussines_ids').html('<option value="">Seleccione una línea de negocio</option>');
		return;
	}
	//ajax
	$.get('/bussines/'+bussine_id+'', function (data) {
		var html_select = '<option value="">Seleccionea línea de negocio</option>';
		for (var i = 0; i<data.length; i++) 
			html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
			$('#list-of-bussines_ids').html(html_select);
	});
}

$(function() {
	$("#company").on('change', onSelectBussineChange);
});

function onSelectBussineChange() {
	var bussine_id = $(this).val();
	if (! bussine_id) {
		$('#list-of-bussines').html('<option value="">Seleccione una línea de negocio</option>');
		return;
	}
	//ajax
	$.get('/bussines/'+bussine_id+'', function (data) {
		var html_select = '<option value="">Seleccionea línea de negocio</option>';
		for (var i = 0; i<data.length; i++) 
			html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
			$('#list-of-bussines').html(html_select);
	});
}
//function reports
$(function() {
	$("#company_id").on('change', onSelectBussineSearchChange);
});

function onSelectBussineSearchChange() {
	var bussine_id = $(this).val();
	if (! bussine_id) {
		$('#bussine_id').html('<option value="">Seleccione una línea de negocio</option>');
		return;
	}
	//ajax
	$.get('/bussines/'+bussine_id+'', function (data) {
		var html_select = '<option value="">Seleccionea línea de negocio</option>';
		for (var i = 0; i<data.length; i++) 
			html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
			$('#bussine_id').html(html_select);
	});
}

