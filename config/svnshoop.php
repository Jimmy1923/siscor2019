<?php

return [
	//default avatar
	'default_avatar'	=> env('DEFAULT_AVATAR') ?: '/images/avatar.png',

	//default icon
	'default_icon'	=> env('DEAFAULT_ICON') ?: 	'/images/favicon.ico',

	//meta
	'meta'	=> [
		'keywords'	=> 'Compras, requesiciones,ventas',
		'description' => 'Nada es imposible en Developersideas',
		'author' 	=> 'F.G.C @Philip'
	],



];